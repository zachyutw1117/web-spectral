module.exports = {
    siteMetadata: {
        title: `Get started with Spectral today`,
        description: `Enabling teams to build and ship software faster while avoiding security mistakes, credential leakage, misconfiguration and data breaches without agents, across the entire software development lifecycle`,
        keywords: ``,
        author: `zachyu.tw@gmail.com`,
        charset: 'UTF-8',
        canonical: '',
        viewport: 'width=device-width, initial-scale=1',
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        // {
        //     resolve: 'gatsby-plugin-react-svg',
        //     options: {
        //         rule: {
        //             include: /assets/, // See below to configure properly
        //         },
        //     },
        // },
        // {
        //     resolve: `gatsby-source-filesystem`,
        //     options: {
        //         name: `assets`,
        //         path: `${__dirname}/src/assets`,
        //     },
        // },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `docs`,
                path: `${__dirname}/src/docs`,
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `gatsby-starter-default`,
                short_name: `starter`,
                start_url: `/`,
                background_color: `#7535FD`,
                theme_color: `#7535FD`,
                display: `minimal-ui`,
                icon: `src/images/SpectralLogo.png`, // This path is relative to the root of the site.
            },
        },
        {
            resolve: `gatsby-plugin-styled-components`,
            options: {
                // Add any options here
            },
        },
        `gatsby-plugin-stylus`,
        `gatsby-plugin-sass`,
        `gatsby-plugin-less`,
        `gatsby-plugin-postcss`,
        'gatsby-plugin-chakra-ui',
        // Add after these plugins if used
        {
            resolve: `gatsby-plugin-purgecss`,
            options: {
                printRejected: true, // Print removed selectors and processed file names
                develop: true, // Enable while using `gatsby develop`
                // tailwind: true, // Enable tailwindcss support
                // whitelist: ['whitelist'], // Don't remove this selector
                // ignore: ['/ignored.css', 'prismjs/', 'docsearch.js/'], // Ignore files/folders
                purgeOnly: ['components/', '/main.css', 'bootstrap/'], // Purge only these files/folders
            },
        },
    ],
};
