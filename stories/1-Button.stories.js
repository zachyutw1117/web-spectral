import React from 'react';
import RadianButton from '../src/components/Button/RadianButton';
import { action } from '@storybook/addon-actions';
export default {
    title: 'Button',
    component: RadianButton,
};

export const renderRadianButton = () => (
    <RadianButton onClick={action('clicked')}>Hello Button</RadianButton>
);

export const renderOutlineRadianButton = () => (
    <RadianButton variant="outline" onClick={action('clicked')}>
        Hello Button
    </RadianButton>
);
export const renderOutlineWhiteRadianButton = () => (
    <div style={{background:'var(--purple-hover)',padding:'30px'}}>
        <RadianButton variant="outline" color="white" onClick={action('clicked')}>
        Hello Button
    </RadianButton>
    </div>
    
);
export const renderOutlineDarkBlueRadianButton = () => (
    <RadianButton variant="outline" color="dark-blue" onClick={action('clicked')}>
        Hello Button
    </RadianButton>
);
