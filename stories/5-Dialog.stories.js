import React from 'react';
import PropTypes from 'prop-types';
import ChakraHubspotFormDialog from '../src/components/Dialog/ChakraHubspotFormDialog';

const Container = ({ children }) => {
    return (
        <div className="container" style={{ background: '#FFF' }}>
            {children}
        </div>
    );
};

Container.propTypes = {
    children: PropTypes.any,
};

export default {
    title: 'Dialog',
    component: ChakraHubspotFormDialog,
};

export const renderChakraHubspotFormDialog = () => (
    <div style={{ width: '100%', height: '100%' }}>
        <ChakraHubspotFormDialog trigger={dProp => <button {...dProp}>text</button>} />
    </div>
);
