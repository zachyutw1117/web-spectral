import React from 'react';
import S02DividerImage from '../src/components/Lotties/S02DividerImage';
import FeatureS02KnowYourBlindspotsImage from '../src/components/Lotties/FeatureS02KnowYourBlindspotsImage';
export default {
    title: 'Lottie',
    component: S02DividerImage,
};

export const renderS02DividerImageLottie = () => <S02DividerImage />;
export const renderFeatureS02KnowYourBlindspotsImage = ()=> <div style={{width:'800px'}}> <FeatureS02KnowYourBlindspotsImage /></div>