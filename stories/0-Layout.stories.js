import React from 'react';
import Header from '../src/components/Header/Header';
import Footer from '../src/components/Footer/Footer';

export default {
    title: 'Layout',
    component: Header,
};

export const renderHeader = () => <div style={{background:'#E5E5E5',height:'100vh',width:'100%'}}> <Header /> </div>;

export const renderFooter = () => <Footer />;
