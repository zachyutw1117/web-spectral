import React from 'react';
import CodeDemo from '../src/components/Code/CodeDemo';
import home from '../src/config/content/home.json';

export default {
    title: 'Code',
    component: CodeDemo,
};

export const renderHeader = () => <CodeDemo codeDemo={home.section.s03.codeDemo} />;
