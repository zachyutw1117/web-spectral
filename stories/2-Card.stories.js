import React from 'react';
import PropTypes from 'prop-types';
import HeaderAnimationCard from '../src/components/Card/HeaderAnimationCard';
import CharacteristicCard from '../src/components/Card/CharacteristicCard';
import EmployeeCard from '../src/components/Card/EmployeeCard';
import CubesBannerCard from '../src/components/Card/CubesBannerCard';
import FindUsCard from '../src/components/Card/FindUsCard';
import FoxChartBannerCard from '../src/components/Card/FoxChartBannerCard';
import IntroductionAnimationCard from '../src/components/Card/IntroductionAnimationCard';
import PromotionCard from '../src/components/Card/PromotionCard';
import StackServiceCard from '../src/components/Card/StackServiceCard';
import NewsGroupCard from '../src/components/Card/NewsGroupCard';
import NewsCard from '../src/components/Card/NewsCard';
import CookieBannerCard from '../src/components/Card/CookieBannerCard';
import FeatureStackCard from '../src/components/Card/FeatureStackCard';

import { action } from '@storybook/addon-actions';
import about from '../src/config/content/about.json';
import home from '../src/config/content/home.json';
import features from '../src/config/content/features.json';
export default {
    title: 'Card',
    component: CharacteristicCard,
};

const Container = ({ children }) => {
    return (
        <div className="container" style={{ background: '#FFF' }}>
            {children}
        </div>
    );
};

Container.propTypes = {
    children: PropTypes.any,
};

export const renderCharacteristicCard = () => (
    <CharacteristicCard data={about.section.s02.characteristic[0]} />
);

export const renderCharacteristicCards = () => (
    <div className="flex evenly" style={{ width: '100%' }}>
        {about.section.s02.characteristic.map((value, index) => (
            <CharacteristicCard key={index} index={index} data={value} />
        ))}
    </div>
);

export const renderEmployeeCard = () => <EmployeeCard data={about.section.s04.employees[0]} />;

export const renderCubesBannerCard = () => (
    <CubesBannerCard
        onClick={action('click')}
        data={{ title: 'Want to join our team?', buttonText: 'Open Positions' }}
    />
);

export const renderFindUsCard = () => <FindUsCard data={about.section.s06} />;
export const renderFoxChartBannerCard = () => <FoxChartBannerCard data={home.section.foxChart} />;

export const renderIntroductionAnimationCard = () => (
    <IntroductionAnimationCard data={home.section.s02.introduction.developerFirst} />
);

export const renderIntroductionAnimationReverseCard = () => (
    <IntroductionAnimationCard data={home.section.s02.introduction.developerFirst} reverse />
);

export const renderHeaderAnimationCard = () => (
    <div style={{ width: '100%', background: '#FFF', paddingTop: '200px' }}>
        <HeaderAnimationCard data={home.section.s01.headerCard} />
    </div>
);

export const renderPromotionCardRowList = () => (
    <Container>
        <div className="flex evenly">
            <PromotionCard data={home.section.s01.promotion.devSecOpsWorkflows} index={0} />
            <PromotionCard data={home.section.s01.promotion.detectMitigateRisk} index={1} />
            <PromotionCard data={home.section.s01.promotion.policiesVisibility} index={2} />
        </div>
    </Container>
);
export const renderPromotionCardColumnList = () => (
    <Container>
        <div className="flex center" style={{ flexDirection: 'column' }}>
            <PromotionCard data={home.section.s01.promotion.devSecOpsWorkflows} index={0} />
            <PromotionCard data={home.section.s01.promotion.detectMitigateRisk} index={1} />
            <PromotionCard data={home.section.s01.promotion.policiesVisibility} index={2} />
        </div>
    </Container>
);

export const renderStackServiceCard = () => <StackServiceCard data={home.section.s04.stack} />;

export const renderNewsCard = () => <NewsCard data={home.section.s05.cardGroup.Elasticsearch} />;

export const renderNewsGroupCard = () => <NewsGroupCard data={home.section.s05} />;

export const renderCookieBannerCard = () => <CookieBannerCard />;

export const renderFeatureStackCard = () => (
    <FeatureStackCard data={features.section.s03.stacks[1]} index={1} />
);
