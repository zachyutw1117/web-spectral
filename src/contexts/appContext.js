import React, { createContext, useState } from 'react';
const Context = createContext({});
export const Provider = props => {
    const [state, setState] = useState({});
    return <Context.Provider value={{ state, setState }}>{props.children}</Context.Provider>;
};

export const withContext = Component => props => {
    return (
        <Provider>
            <Component {...props} />
        </Provider>
    );
};
export default Context;
