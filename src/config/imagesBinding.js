const importAll = require =>
    require.keys().reduce((acc, next) => {
        acc[next.replace('./', '').replace(/\.(png|jpe?g|svg)/, '')] = require(next);
        return acc;
    }, {});

let images = {};
images = importAll(require.context('../images', false, /\.(png|jpe?g|svg)$/));
console.log(images);
export const getImage = imageSrc => {
    return images[imageSrc];
};

export default images;
