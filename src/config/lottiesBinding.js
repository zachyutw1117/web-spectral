import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Lottie from '../components/Lotties/LottieLoader';


const defaultOptions = {
    loop: true,
    autoplay: true,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
        filterSize: {
                width: '200%',
                height: '200%',
                x: '-50%',
                y: '-50%',
            },
    },
};

const Wrapper = styled.div`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    & > div {
        height: auto !important;
    }
`;

const LottieComponent = ({ animationData }) => {
    return (
        <Wrapper className="lottie">
            <Lottie options={{ ...defaultOptions, animationData }} speed={0.5} />
        </Wrapper>
    );
};
LottieComponent.propTypes = {
    animationData: PropTypes.object
}

const importAll = require =>
    require.keys().reduce((acc, next) => {
        acc[next.replace('./', '').replace(/\.(json)/, '')] = require(next);
        return acc;
    }, {});

let lotties = {};
lotties = importAll(require.context('../lotties', false, /\.(json)$/));

export const renderLotties = Object.fromEntries(
    Object.entries(lotties).map(([keyName, animationData]) => [
        keyName,
        React.createElement(LottieComponent, { animationData }),
    ])
);

export default lotties;
