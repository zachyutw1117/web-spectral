import React from 'react';
import s01Image from '../components/Image/S01Image';
import S02DevFirstImage from '../components/Image/S02DevFirstImage';
import S02ProductionImage from '../components/Image/S02ProductionImage';
import S02DividerImage from '../components/Image/S02DividerImage';
import Promotion1 from '../components/Image/PromotionImage1';
import Promotion2 from '../components/Image/PromotionImage2';
import Promotion3 from '../components/Image/PromotionImage3';
const lazyImages = {};

lazyImages['s01Image'] = React.createElement(s01Image);
lazyImages['S02DevFirstImage'] = React.createElement(S02DevFirstImage);
lazyImages['S02ProductionImage'] = React.createElement(S02ProductionImage);
lazyImages['S02DividerImage'] = React.createElement(S02DividerImage);
lazyImages['Promotion1'] = React.createElement(Promotion1);
lazyImages['Promotion2'] = React.createElement(Promotion2);
lazyImages['Promotion3'] = React.createElement(Promotion3);

export default lazyImages;
