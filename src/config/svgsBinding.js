// import React from 'react';
let svgs = {};
const importAllSvgReactComponent = require =>
    require.keys().reduce((acc, next) => {
        acc[next.replace('./', '').replace(/\.(png|jpe?g|svg)/, '')] = require(next);
        return acc;
    }, {});

svgs = importAllSvgReactComponent(require.context('../assets', false, /\.(png|jpe?g|svg)$/));
export const getSvg = imageSrc => {
    return svgs[imageSrc];
};

export default svgs;
