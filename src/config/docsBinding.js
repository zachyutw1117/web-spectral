const importAllDocs = require =>
    require.keys().reduce((acc, next) => {
        acc[next.replace('./', '').replace(/\.(pdf)/, '')] = require(next);
        return acc;
    }, {});

const docs = importAllDocs(require.context(`../docs`, false, /\.(pdf)$/));

export default docs;
