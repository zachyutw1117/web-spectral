/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react';
import PropTypes from 'prop-types';
// import { useStaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

import Header from './Header/Header';
import Footer from './Footer/Footer';
import PageLoader from './Loader/PageLoader';
import '../styles/index.css';
import '../styles/index.scss';
import 'hover.css/css/hover-min.css';

const Wrapper = styled.div`
    width: 100%;
    overflow-x: hidden;
`;
const LayoutContent = styled.div``;

const Layout = ({ children }) => {
    // const data = useStaticQuery(graphql`
    //     query SiteTitleQuery {
    //         site {
    //             siteMetadata {
    //                 title
    //             }
    //         }
    //     }
    // `);

    return (
        <Wrapper>
            <Header />
            <LayoutContent>{children} </LayoutContent>
            <Footer />
            <PageLoader />
        </Wrapper>
    );
};

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;
