import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import {
    AlertDialog,
    AlertDialogOverlay,
    AlertDialogContent,
    AlertDialogBody,
} from '@chakra-ui/core';
import styled from 'styled-components';
import svgs from '../../config/svgsBinding';
// import useHubspotForm from '@aaronhayes/react-use-hubspot-form';
import HubspotFormReact from 'react-hubspot-form';

const DialogContentWrapper = styled(AlertDialogContent)`
    background: rgb(245, 248, 250) !important;
    overflow: hidden;
    border-radius: 10px;
    max-width: 800px !important;
    position: relative;
    padding-top: 1.5rem;
    @media (max-width: 1440px) {
    }
    @media (max-width: 1024px) {
    }
    @media (max-width: 769px) {
        max-width: 80vw;
    }
    @media (max-width: 481px) {
        max-width: 94vw;
    }
    @media (max-width: 376px) {
    }
    .close_icon {
        position: absolute;
        right: 15px;
        top: 15px;
        height: 32px;
        width: 32px;
        cursor: pointer;
        z-index: 8888;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            left: 7px;
            top: 7px;
        }
        @media (max-width: 376px) {
        }
    }
    .close_button {
        position: absolute;
        bottom: 133px;
        left: 30%;
        z-index: 2;
        margin: 0;
        cursor: pointer;
        display: inline-block;
        font-weight: 700;
        line-height: 12px;
        text-align: center;
        color: #fff;
        border-radius: 3px;
        font-size: 14px;
        padding: 12px 24px;
        background: #fff;
        border: 1px solid #762aeb;
        color: #762aeb;
        font-size: 12px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            left: 40%;
        }
        @media (max-width: 376px) {
            left: 50%;
        }
    }
`;

const DialogBodyWrapper = styled(AlertDialogBody)`
    background: rgb(245, 248, 250);
    /* padding-top:0 !important; */
    padding-bottom: 0 !important;
    width: 100%;
    height: 780px;
    min-height: 780px;

    display: flex;
    justify-content: center;
    align-items: center;

    iframe, form {
        z-index: 1;
        width: 100%;
        height: 100%;
        /* background:url('${svgs['LoaderImage']}') center center no-repeat; */
        /* background:url('https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif') center center no-repeat; */
        /* margin-top:-50px; */
    }
    #hs-form-iframe-0, #hs-form-iframe-1, #hs-form-iframe-2 {
        width:600px !important;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            width: 80vw !important;
        }
        @media (max-width: 481px) {
            width: 80vw !important;
        }
        @media (max-width: 376px) {
        }
    }
`;

const hubspotConfig = {
    portalId: '7649059',
    formId: 'd3fb15bf-e4b8-4c85-afa6-7cefc8d2dcb5',
    target: '#hubspot-form',
};

const ChakraHubspotFormDialog = ({ trigger }) => {
    const [isOpen, setIsOpen] = useState(false);
    // const [isLoading, setIsLoading] = useState(true);
    const onClose = () => setIsOpen(false);
    const onOpen = () => setIsOpen(true);
    const cancelRef = useRef();

    return (
        <>
            {trigger({ onClick: onOpen })}
            <AlertDialog isOpen={isOpen} leastDestructiveRef={cancelRef} onClose={onClose}>
                <AlertDialogOverlay />
                <DialogContentWrapper style={{ alignItems: 'center' }}>
                    <DialogBodyWrapper>
                        {/* { isLoading && <Spinner size="xl" /> } */}
                        {/* <iframe src="https://share.hsforms.com/10_sVv-S4TIWvpnzvyNLctQ4jy1v" name="hubspotfrom" title="hubspotfrom" /> */}
                        <img
                            className="close_icon"
                            src={svgs['XIconPurple']}
                            alt="x"
                            onClick={() => {
                                setIsOpen(false);
                            }}
                        />
                        <HubspotFormReact {...hubspotConfig} loading={<div>Loading...</div>} />
                        {/* <form id="hubspot-form" /> */}
                    </DialogBodyWrapper>
                </DialogContentWrapper>
            </AlertDialog>
        </>
    );
};

ChakraHubspotFormDialog.propTypes = {
    trigger: PropTypes.func,
};

export default ChakraHubspotFormDialog;
