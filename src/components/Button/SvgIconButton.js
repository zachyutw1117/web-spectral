import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import svgs from '../../config/svgsBinding';

const Wrapper = styled.span`
    padding: 1rem;
    font-size: 1.5rem;
    cursor: pointer;
    color: var(--text-link);
`;

const SvgIconButton = ({ icon='', onClick=()=>{},...rest})=><Wrapper onClick={onClick} className="menu-btn" {...rest}>
<img height={40} width={40} src={svgs[icon]} alt="menu" />
</Wrapper>

SvgIconButton.propTypes = {
    onClick: PropTypes.func,
    icon:PropTypes.string
}

export default SvgIconButton;