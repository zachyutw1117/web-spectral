import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Link } from 'gatsby';

const BUTTON_CLASS_NAME = 'radian-button';
const Wrapper = styled.button``;

const RadianButton = ({ children, className, variant, color, to, href, text, ...rest }) => {
    const clsxClass = clsx(BUTTON_CLASS_NAME, 'radian button', className, variant, color);

    if (to) {
        return (
            <Link className={clsxClass} to={to} {...rest}>
                {text}
                {children}
            </Link>
        );
    }
    if (href) {
        return (
            <a className={clsxClass} href={href} target="_blank" rel="noreferrer" {...rest}>
                {text}
                {children}
            </a>
        );
    }

    return (
        <Wrapper className={clsxClass} {...rest}>
            {text}
            {children}
        </Wrapper>
    );
};

RadianButton.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    to: PropTypes.string,
    href: PropTypes.string,
    text: PropTypes.string,
    variant: PropTypes.oneOf(['outline', 'solid']),
    color: PropTypes.oneOf(['white', 'dark-blue']),
};

export default RadianButton;
