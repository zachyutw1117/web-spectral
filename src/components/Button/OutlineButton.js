import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
const Wrapper = styled.button`
    text-transform: capitalize;
    border-radius: 32px !important;
    text-transform: capitalize;
    border: 1px solid var(--purple) !important;
    background: var(--text-light) !important;
    color: var(--purple) !important;
    cursor: pointer;
    &:hover {
        border: 1px solid var(--purple-hover) !important;
        color: var(--purple-hover) !important;
    }
    &.white {
        border: 2px solid var(--text-light) !important;
        background: transparent !important;
        color: var(--text-light) !important;
    }
`;

const OutlineButton = ({ children, className, ...rest }) => {
    return (
        <Wrapper className={clsx('button', className)} {...rest}>
            {children}
        </Wrapper>
    );
};

OutlineButton.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};

export default OutlineButton;
