import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
const Wrapper = styled.button`
    border-radius: 32px;
    text-transform: capitalize;
    border: 3px solid var(--purple);
    background: var(--purple);
    color: var(--text-light);
    box-shadow: 0px 8px 32px rgba(117, 53, 253, 0.24);
    cursor: pointer;
    &:hover {
        background: var(--purple-hover);
    }
`;

const SolidButton = ({ children, className, ...rest }) => {
    return (
        <Wrapper className={clsx('button', className)} variant="solid" {...rest}>
            {children}
        </Wrapper>
    );
};

SolidButton.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};

export default SolidButton;
