import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.button`
    border-radius: 32px !important;
    border: 1.5px solid var(--text-link) !important;
    background-color: transparent !important;
    color: var(--text-link) !important;
    letter-spacing: 0.04em;
    padding: 16px 32px;
    font-family: 'Inter';
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 110%;
    /* or 15px */
    display: flex;
    align-items: center;
    letter-spacing: 0.02em;
    text-transform: uppercase;
`;
const SignUpButton = () => {
    return <Wrapper>Sign Up</Wrapper>;
};

export default SignUpButton;
