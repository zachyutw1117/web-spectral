import React from 'react';
import Lottie from './LottieLoader';
import styled from 'styled-components';
import animationData from '../../lotties/FeatureS02KnowYourBlindspotsImage.json';

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
        filterSize: {
                width: '200%',
                height: '200%',
                x: '-50%',
                y: '-50%',
            },
    },
};

const Wrapper = styled.div`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: auto;
`;

const S02DividerImageLottie = () => {
    return (
        <Wrapper className="lottie">
            <Lottie options={defaultOptions} speed={0.2} />
        </Wrapper>
    );
};

export default S02DividerImageLottie;
