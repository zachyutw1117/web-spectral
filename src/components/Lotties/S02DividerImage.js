import React from 'react';
import Lottie from 'react-lottie';
import styled from 'styled-components';
import animationData from '../../lotties/S02DividerImage.json';

const defaultOptions = {
    loop: true,
    autoplay: false,
    animationData,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
        filterSize: {
                width: '200%',
                height: '200%',
                x: '-50%',
                y: '-50%',
            },
    },
};

const Wrapper = styled.div`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: auto;
`;

const S02DividerImageLottie = () => {
    return (
        <Wrapper className="lottie">
            <Lottie options={defaultOptions} />
        </Wrapper>
    );
};

export default S02DividerImageLottie;
