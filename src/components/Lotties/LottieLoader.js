import React, { useEffect, useState, useRef, useMemo } from 'react';
import PropTypes from 'prop-types';
import lottie from 'lottie-web';

import useCheckIsInViewport from '../../hooks/useCheckIsInViewport';

const getSize = initial => {
    let size;

    if (typeof initial === 'number') {
        size = `${initial}px`;
    } else {
        size = initial || '100%';
    }

    return size;
};

const registerEvents = (eventListeners = [], node) => {
    eventListeners.forEach(eventListener => {
        node.addEventListener(eventListener.eventName, eventListener.callback);
    });
};
const deRegisterEvents = (eventListeners = [], node) => {
    eventListeners.forEach(eventListener => {
        node.removeEventListener(eventListener.eventName, eventListener.callback);
    });
};

const Lottie = ({
    options: propOptions,
    eventListeners,
    style,
    isStopped = false,
    isPaused = false,
    width,
    height,
    speed = 1,
    ariaRole = 'button',
    ariaLabel = 'animation',
    isClickToPauseDisabled = false,
    direction,
    segments,
    title = '',
}) => {
    const { loop, autoplay, animationData, rendererSettings } = propOptions;
    const [anim, setAnim] = useState(null);
    const [options, setOptions] = useState({
        renderer: 'svg',
        loop: loop !== false,
        autoplay: autoplay !== false,
        segments: segments !== false,
        animationData,
        rendererSettings,
        ...propOptions,
    });
    const ref = useRef(null);
    const isInViewport = useCheckIsInViewport(ref);

    useEffect(() => {
        if (anim) {
            if (isInViewport) {
                anim.play();
            } else {
                anim.stop();
            }
        }
    }, [isInViewport, anim]);

    useEffect(
        () => {
            const _options = { ...options, container: ref.current };
            const _anim = lottie.loadAnimation(_options);
            if (speed) {
                _anim.setSpeed(speed);
            }
            setOptions(_options);
            setAnim(_anim);
            registerEvents(eventListeners, _anim);

            return () => {
                deRegisterEvents(eventListeners, _anim);

                _anim.destroy();
                setOptions(value => ({ ...value, animationData: null }));
                setAnim(null);
            };
        },
        []
    );

    useEffect(() => {
        if (anim) {
            if ((isPaused && !anim.isPaused) || (!isPaused && anim.isPaused)) {
                anim.pause();
            }
        }
    }, [isPaused, anim]);

    useEffect(() => {
        if (isStopped && anim) {
            anim.stop();
        }
    }, [isStopped, anim]);

    useEffect(() => {
        if (segments && anim) {
            anim.playSegments(segments);
        }
    }, [segments, anim]);

    useEffect(() => {
        if (direction && anim) {
            anim.setDirection(direction);
        }
    }, [direction, anim]);

    useEffect(() => {
        if (speed && anim) {
            anim.setSpeed(speed);
        }
    }, [speed, anim]);

    const handleClickToPause = () => {
        // The pause() method is for handling pausing by passing a prop isPaused
        // This method is for handling the ability to pause by clicking on the animation
        if (anim) {
            if (anim.isPaused) {
                anim.play();
            } else {
                anim.pause();
            }
        }
    };

    const onClickHandler = isClickToPauseDisabled ? () => null : handleClickToPause;
    const lottieStyles = useMemo(() => {
        return {
            width: getSize(width),
            height: getSize(height),
            overflow: 'hidden',
            margin: '0 auto',
            outline: 'none',
            zIndex:0,
            ...style,
        };
    }, [width, height, style]);

    return (
        <div
            ref={ref}
            style={lottieStyles}
            data-isinviewport={isInViewport}
            onClick={onClickHandler}
            title={title}
            role={ariaRole}
            aria-label={ariaLabel}
            tabIndex="0"
        />
    );
};

export default Lottie;

Lottie.propTypes = {
    eventListeners: PropTypes.arrayOf(PropTypes.object),
    options: PropTypes.object.isRequired,
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isStopped: PropTypes.bool,
    isPaused: PropTypes.bool,
    speed: PropTypes.number,
    segments: PropTypes.arrayOf(PropTypes.number),
    direction: PropTypes.number,
    ariaRole: PropTypes.string,
    ariaLabel: PropTypes.string,
    isClickToPauseDisabled: PropTypes.bool,
    title: PropTypes.string,
    style: PropTypes.string,
};

Lottie.defaultProps = {
    eventListeners: [],
    isStopped: false,
    isPaused: false,
    speed: 1,
    ariaRole: 'button',
    ariaLabel: 'animation',
    isClickToPauseDisabled: false,
    title: '',
};
