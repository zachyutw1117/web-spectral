import React from 'react';
import Lottie from 'react-lottie';
import styled from 'styled-components';

import config from '../../config';
const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: config.lotties['S01Image'],
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
    },
};

const Wrapper = styled.div`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    /* padding-top:340px;
    padding-bottom:356px;
    padding-right:200px;
    padding-left:424.76px;
    @media(max-width:1024px){
        padding-top:340px;
        padding-bottom:356px;
        padding-right:200px;
        padding-left:424.76px;
    } */
    /* max-width: 2360.76px;
    max-height: 1362.99px;
    width:100%;
    height:auto; */
`;

const S01ImageLottie = () => {
    return (
        <Wrapper className="lottie">
            <Lottie options={defaultOptions} />
        </Wrapper>
    );
};

export default S01ImageLottie;
