import React from 'react';
import Lottie from 'react-lottie';
import styled from 'styled-components';

import config from '../../config';
const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: config.lotties['S02ProductionImage'],
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice',
    },
};

const Wrapper = styled.div`
    display: inline-flex;
    justify-content: center;
    align-items: center;
`;

const S01ImageLottie = () => {
    return (
        <Wrapper className="lottie">
            <Lottie options={defaultOptions} />
        </Wrapper>
    );
};

export default S01ImageLottie;
