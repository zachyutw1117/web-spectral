import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import CookieBannerCard from '../Card/CookieBannerCard';
import FooterNav from './FooterNav';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import config from '../../config';
const { footer } = config.settings;

const FooterWrapper = styled.footer`
    z-index: 5;
    background-color: var(--dark-back);
    .footer-container {
        color: var(--text-light);
        display: flex;
        align-items: flex-start;
        flex-direction: row;

        padding: 65px 205px 50px 205px;

        @media (max-width: 1440px) {
            padding: 65px 205px 50px 205px;
        }
        @media (max-width: 1024px) {
            padding: 65px 140px 50px 140px;
        }
        @media (max-width: 769px) {
            flex-direction: column;
            padding: 65px 120px 50px 120px;
        }
        @media (max-width: 481px) {
            padding: 85px 60px 112px 60px;
        }
        @media (max-width: 376px) {
            padding: 85px 10px 50px 20px;
        }
        &.isOpen {
            padding-bottom: 165px;
            @media (max-width: 1440px) {
                padding: 65px 205px 165px 205px;
            }
            @media (max-width: 1024px) {
                padding: 65px 140px 165px 140px;
            }
            @media (max-width: 769px) {
                flex-direction: column;
                padding: 65px 120px 165px 120px;
            }
            @media (max-width: 481px) {
                padding: 85px 68px 160px 68px;
            }
            @media (max-width: 376px) {
                padding: 85px 10px 200px 20px;
            }
        }

        a {
            color: var(--text-light);
        }

        .footer-logo {
            flex: 1 1 20%;
            padding: 24px 108px 24px 24px;
            display: flex;
            flex-direction: column;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                padding: 24px 55px 24px 24px;
            }
            @media (max-width: 769px) {
                flex: 1 1 100%;
                padding: 71px 24px 30px 24px;
                width: 100%;
                justify-content: flex-start;
                align-items: center;
                text-align: center;
            }
            @media (max-width: 481px) {
                padding: 10px 24px 20px 24px;
            }
            @media (max-width: 376px) {
                padding: 5px 24px 15px 24px;
            }
            .footer-copyright {
                margin-top: 12px;
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 12px;
                line-height: 150%;
                /* identical to box height, or 18px */
                letter-spacing: 0.02em;
                width: 200px;
            }
        }
    }
`;

const Footer = () => {
    const localId = 'acceptCookie';
    const [isOpen, setIsOpen] = useState(false);
    useEffect(() => {
        const acceptCookie = localStorage.getItem(localId);
        if (acceptCookie) {
            setIsOpen(false);
        } else {
            setIsOpen(true);
        }
    }, []);

    const handleOnAccept = () => {
        setIsOpen(false);
        localStorage.setItem(localId, 'true');
    };
    const handleOnClose = () => {
        setIsOpen(false);
    };
    return (
        <FooterWrapper className={clsx('footer')} data-testid="footer">
            <div className={clsx('container footer-container', { isOpen })}>
                <div className="footer-logo">
                    <div>
                        <img src={config.svgs['FooterLogoImage']} alt="logo" />
                    </div>
                    <p className="footer-copyright"> {footer.copyright} </p>
                </div>
                <FooterNav nav={footer.nav} />
                <CookieBannerCard
                    isOpen={isOpen}
                    onClose={handleOnClose}
                    onAccept={handleOnAccept}
                />
            </div>
        </FooterWrapper>
    );
};

Footer.propTypes = {
    logoSrc: PropTypes.string,
    copyright: PropTypes.string,
    nav: PropTypes.object,
};

export default Footer;
