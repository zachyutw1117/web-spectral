import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';
import clsx from 'clsx';

import PropTypes from 'prop-types';
import config from '../../config';
import svgs from '../../config/svgsBinding';

const LINK_CLASS_NAME = 'footer-nav-link';

const Wrapper = styled.div`
    margin: 16px 0;
    a.${LINK_CLASS_NAME} {
        max-width: 200px;
        display: inline-flex;
        align-items: center;
        font-family: Inter;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 150%;
        /* identical to box height, or 24px */
        letter-spacing: 0.02em;
        img {
            margin: 0 8px 0 0;
            width: 20px;
            height: 20px;
        }
    }
`;

const FooterNavLink = ({ linkName, data: { to = '/', href, type, icon } }) => {
    const className = clsx(LINK_CLASS_NAME);

    if (type === 'docs') {
        return (
            <Wrapper>
                <a className={className} href={config.docs[href]}>
                    {icon && <img height={24} width={24} src={svgs[icon]} />}
                    {` ${linkName}`}
                </a>
            </Wrapper>
        );
    }
    if (href) {
        return (
            <Wrapper>
                <a className={className} href={href}>
                    {icon && <img height={24} width={24} src={svgs[icon]} />}
                    {` ${linkName}`}
                </a>
            </Wrapper>
        );
    }
    return (
        <Wrapper>
            <Link className={className} to={to} href={href}>
                {icon && <img height={24} width={24} src={svgs[icon]} />}
                {`${linkName}`}
            </Link>
        </Wrapper>
    );
};

FooterNavLink.propTypes = {
    linkName: PropTypes.string,
    data: PropTypes.shape({
        icon: PropTypes.string,
        to: PropTypes.string,
        href: PropTypes.string,
        type: PropTypes.string,
    }),
};

export default FooterNavLink;
