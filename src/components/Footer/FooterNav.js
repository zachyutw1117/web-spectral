import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';

import FooterNavLink from './FooterNavLink';
import PropTypes from 'prop-types';

const NAV_CLASS_NAME = 'footer-nav';

const Wrapper = styled.nav`
    &.${NAV_CLASS_NAME} {
        flex: 1 1 100%;
        display: flex;
        justify-content: space-between;
        padding: 14px;
        width: 100%;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            flex-wrap: wrap;
        }
        @media (max-width: 376px) {
        }
        .footer-nav-section {
            flex: 1 1 100%;
            text-transform: capitalize;
            line-height: 150%;
            letter-spacing: 0.02em;
            margin: 12px 0;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                width: 120px;
                max-width: 120px;
            }
            @media (max-width: 481px) {
                margin: 12px 0 37px 0;
            }
            @media (max-width: 376px) {
            }
        }
        .footer-nav-title-head {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 800;
            font-size: 16px;
            line-height: 150%;
            /* identical to box height, or 24px */

            letter-spacing: 0.02em;
            text-transform: capitalize;
        }
    }
`;

const FooterNav = ({ nav }) => (
    <Wrapper className={clsx(NAV_CLASS_NAME)}>
        {Object.entries(nav).map(([name, { nav: navTitle = {} }]) => (
            <div key={name} className="footer-nav-section">
                <div className="footer-nav-title-head">{name}</div>
                {Object.entries(navTitle).map(([linkName, value]) => (
                    <FooterNavLink key={linkName} linkName={linkName} data={value} />
                ))}
            </div>
        ))}
    </Wrapper>
);

FooterNav.propTypes = {
    nav: PropTypes.object,
};

export default FooterNav;
