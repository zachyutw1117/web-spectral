import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import ServicesIcon from '../Icon/ServicesIcon';

const CARD_CLASS_NAME = 'stack-service-card';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        position: relative;
        flex-direction: column;
        padding: 20px;
        width: 100%;
        max-width: 882px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            flex-direction: row;
            align-items: stretch;
        }
        @media (max-width: 376px) {
        }
        .stack-content {
            width: 100%;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                flex-direction: column;
                justify-content: space-around;
                align-items: unset;
            }
            @media (max-width: 376px) {
            }
        }
        .title {
            font-weight: 600;
            font-size: 16px;
            text-transform: uppercase;
            margin-bottom: 15px;
        }
        .stack-diver-border {
            height: 2px;
            background: linear-gradient(270deg, #18bbf1 0%, #4284fb 106.32%);
            width: 100%;
            position: relative;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                display: flex;
                height: 566px;
                width: 6px;
            }
            @media (max-width: 376px) {
            }
            .icons-group {
                position: absolute;
                top: -11px;
                left: 0;
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: space-evenly;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                }
                @media (max-width: 481px) {
                    flex-direction: column;
                    & > span {
                        transform: rotate(90deg) translateY(0px) translateX(12px);
                    }
                }
                @media (max-width: 376px) {
                }
            }
        }

        img.stack-divider {
            position: relative;
            width: 100%;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                display: none;
            }
            @media (max-width: 376px) {
            }
        }
        img.stack-vertical-divider {
            display: none;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                display: flex;
                height: 100%;
            }
            @media (max-width: 376px) {
            }
        }
        .stack {
            display: flex;
            flex: 1 1;
            flex-direction: column;
            align-items: center;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                flex-direction: row;
                justify-content: center;
                .title {
                    margin: 0;
                }
            }
            @media (max-width: 376px) {
            }
            .stack-services {
                position: relative;
                /* &:after {
                    content: '▶';
                    position: absolute;
                    top: -10px;
                    right: 0;
                    width: 20px;
                    height: 8px;
                    color: #4284fb;
                } */
                flex-wrap: wrap;
                /* border-top: 1px solid #4284fb; */
                margin-top: 5px;
                padding-top: 8px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                }
                @media (max-width: 481px) {
                    margin-top: 0;
                    padding-top: 0;
                }
                @media (max-width: 376px) {
                }
                & > .stack-service-icon:nth-child(odd) {
                    justify-content: flex-end;
                }
                & > .stack-service-icon:nth-child(even) {
                    justify-content: flex-start;
                }
                .stack-service-icon {
                    display: flex;

                    width: 50%;
                }
            }
        }
    }
`;
const StackServiceCard = ({ data }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME, 'flex center')}>
            <div className="stack-content flex evenly">
                {Object.values(data).map(({ name }) => (
                    <div key={name} className="stack">
                        <div className="title">{name}</div>
                    </div>
                ))}
            </div>
            {/* <img className="stack-vertical-divider" src={svgs['Home05VerticalDividerImage']} /> */}
            <div className="stack-diver-border">
                <div className="icons-group">
                    <span style={{ color: '#3B95F8' }}>►</span>
                    <span style={{ color: '#36A1F7' }}>►</span>
                    <span style={{ color: '#2CAFF5' }}>►</span>
                </div>
            </div>
            {/* <img className="stack-divider" src={svgs['Home05Divider']} /> */}
            <div className="stack-content flex evenly">
                {Object.values(data).map(({ name, service }) => (
                    <div key={name} className="stack">
                        <div className="stack-services flex">
                            {Object.values(service).map(({ icon }, index) => (
                                <div className="stack-service-icon" key={index}>
                                    <ServicesIcon icon={icon} />
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>
        </Wrapper>
    );
};

StackServiceCard.propTypes = {
    data: PropTypes.shape({
        name: PropTypes.string,
        service: PropTypes.string,
    }),
    index: PropTypes.number,
};

export default StackServiceCard;
