import React, { useMemo } from 'react';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import _ from 'lodash';

import NewsCard from './NewsCard';
const CARD_CLASS_NAME = 'news-card-group';

const RollDown = keyframes`
    0% {
        transform: translateZ(0) translateY(20%);
    }
    100%{
        transform: translate3d(0,-50%,0) translateY(20%);; 
    }
`;

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        padding: 25px 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            padding: 25px 14px 70px 14px;
            height:810px;
            overflow:hidden;
        }
        @media (max-width: 481px) {
            padding: 25px 14px 70px 14px;
            height:830px;
        }
        @media (max-width: 376px) {
            height:1100px;
        }
        &:hover {
            .column {
                .column-items > *:not(:hover) {
                    opacity: 0.5;
                }
                animation-play-state: paused;
            }
        }

        & > .column:nth-child(odd) {
            /* transform: translateZ(0) translateY(20%); */
            will-change: transform;
            /* animation-name: ${RollDown}; */
            animation-duration: 60s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            animation-direction: alternate;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                transform: translateY(50px);
                animation-name: unset;
                /* animation-play-state: paused; */
            }
            @media (max-width: 481px) {
                transform: translateY(0);
            }
            @media (max-width: 376px) {
            }
        }
        & > .column:nth-child(even) {
            /* transform: translateZ(0) translateY(20%); */
            will-change: transform;
            /* animation-name: ${RollDown}; */
            animation-duration: 60s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            animation-direction: alternate-reverse;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                animation-name: unset;
                transform: translateY(-50px);
                /* animation-play-state: paused; */
            }
            @media (max-width: 481px) {
                transform: translateY(0);
            }
            @media (max-width: 376px) {
            }
        }
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            flex-direction: column;
        }
        @media (max-width: 376px) {
        }
    }
`;
const NewsGroupCard = ({ data: { cardGroup, blockNum = 2 } }) => {
    const columns = useMemo(() => {
        return _.chunk(
            Object.values(cardGroup),
            Math.round(Object.values(cardGroup).length / blockNum)
        );
    }, [cardGroup, blockNum]);

    return (
        <Wrapper className={clsx(CARD_CLASS_NAME)}>
            {columns.map((row = [], columnIndex) => (
                <div key={columnIndex} className="column">
                    <div className="column-items ">
                        {row.map((value, rowIndex) => (
                            <NewsCard
                                index={rowIndex + columnIndex * 3}
                                key={rowIndex}
                                data={value}
                            />
                        ))}
                    </div>
                </div>
            ))}
        </Wrapper>
    );
};

NewsGroupCard.propTypes = {
    data: PropTypes.shape({
        cardGroup: PropTypes.object,
        blockNum: PropTypes.number,
    }),
};

export default NewsGroupCard;
