import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import RadianButton from '../Button/RadianButton';
import ChakraHubspotFormDialog from '../Dialog/ChakraHubspotFormDialog';
import config from '../../config';

const CARD_CLASS_NAME = 'cubes-banner-card';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        position: relative;
        width: 100%;
        z-index: 1;
        text-align: center;
        position: relative;
        background: #7535fd;
        color: #fff;

        box-shadow: 0px 8px 40px rgba(117, 53, 253, 0.32);

        overflow: hidden;
        box-shadow: 0px 8px 40px var(--purple-shadow);
        border-radius: 16px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            padding: 28px;
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            border-radius: 0;
        }
        @media (max-width: 376px) {
        }

        .content {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 362px;
            z-index: 1;
            h2 {
                z-index: 1;
            }
            button,
            a {
                z-index: 1;
            }
            img.arrow {
                z-index: 1;
                margin: 4px 0 23px 0;
            }
        }
        .bg {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 0;

            & > .gradient-bg {
                background-image: linear-gradient(
                    265.37deg,
                    rgba(193, 164, 255, 0.46) 0%,
                    rgba(193, 164, 255, 0) 40%,
                    rgba(193, 164, 255, 0) 60%,
                    rgba(193, 164, 255, 0.46) 100%
                );
                width: 100%;
                height: 100%;
                opacity: 0.5;
            }
            img.leftCubes {
                position: absolute;
                bottom: 0;
                left: 0;
            }
            img.rightCubes {
                position: absolute;
                bottom: 0;
                right: 0;
            }
        }
    }
`;

const CubesBannerCard = ({ data: { title, buttonText, href, isHubSpot }, onClick }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME)}>
            <div className="bg">
                <div className="gradient-bg" />
                <img
                    className="leftCubes"
                    src={config.svgs['FeatureSpectralTodayLeftCubes']}
                    alt="left cubes"
                />
                <img
                    className="rightCubes"
                    src={config.svgs['FeatureSpectralTodayRightCubes']}
                    alt="right cubes"
                />
            </div>
            <div className="content">
                <h2>{title}</h2>
                <img
                    className="arrow"
                    src={config.svgs['FeatureSpectralTodayArrowImage']}
                    alt="arrow"
                />

                {href && (
                    <RadianButton onClick={onClick} href={href} variant="outline" color="white">
                        {buttonText}
                    </RadianButton>
                )}
                {isHubSpot && (
                    <ChakraHubspotFormDialog
                        trigger={dialog => (
                            <RadianButton variant="outline" color="white" {...dialog}>
                                {buttonText}
                            </RadianButton>
                        )}
                    />
                )}
            </div>
        </Wrapper>
    );
};

CubesBannerCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string,
        buttonText: PropTypes.string,
        href: PropTypes.string,
        isHubSpot: PropTypes.bool,
    }),
    onClick: PropTypes.func,
};

export default CubesBannerCard;
