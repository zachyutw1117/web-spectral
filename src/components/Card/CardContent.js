import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import RadianButton from '../Button/RadianButton';

const CARD_CLASS_NAME = 'card-content';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        width: 100%;
        margin: 20px 0;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            margin: 0px;
            max-width: calc(100% - 64px);
            padding: 0;
        }
        @media (max-width: 769px) {
            align-items: center;
            margin: 30px 0;
            padding: 0 32px;
            max-width: 100%;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        h2 {
            font-size: 48px;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                font-size: 40px;
                margin-top: 0;
            }
            @media (max-width: 769px) {
                font-size: 40px;
                margin: 8px 0;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
                font-size: 36px;
            }
        }
        p {
            font-size: 24px;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                width: 400px;
            }
            @media (max-width: 769px) {
                font-size: 20px;
            }
            @media (max-width: 481px) {
                font-size: 18px;
            }
            @media (max-width: 376px) {
                font-size: 16px;
            }
        }
        .button {
            padding: 14px 32px !important;
            font-size: 16px !important;
            font-weight: bold;
            margin-top: 32px;
            font-family: Inter;
            font-style: normal;

            line-height: 110%;
            /* identical to box height, or 18px */
            display: flex;
            align-items: center;
            letter-spacing: 0.02em;
            text-transform: capitalize;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                /* margin-top: 17px; */
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
                font-size: 14px !important;
            }
        }
    }
`;
const IntroductionAnimationCard = ({
    data: { title, description, buttonText, href, to },
    color,
}) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME, { color })}>
            <h2>{title}</h2>
            <p>{description}</p>
            {buttonText && (
                <RadianButton href={href} to={to} variant="solid">
                    {buttonText}
                </RadianButton>
            )}
        </Wrapper>
    );
};

IntroductionAnimationCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string,
        description: PropTypes.string,
        buttonText: PropTypes.string,
        href: PropTypes.string,
        to: PropTypes.to,
    }),
    color: PropTypes.string,
};

export default IntroductionAnimationCard;
