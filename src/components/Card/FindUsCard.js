import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import images from '../../config/imagesBinding';

const CARD_CLASS_NAME = 'find-us-card';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        flex-direction: row;
        background: #ffffff;
        box-shadow: 0px 8px 180px rgba(17, 113, 167, 0.66);
        border-radius: 8px;
        width: 100%;
        max-width: 969px;
        overflow: hidden;
        @media (max-width: 769px) {
            flex-direction: column;
        }
        & > img {
            width: 290px;
            height: 475px;
            object-fit: cover;
           
            @media (max-width: 769px) {
                width: 100%;
                height: 272px;
                object-fit: cover;
            }
        }
        & > .img {
            width: 290px;
            height: 475px;
            min-width:290px;
            background-image: url('${images['AboutHowToFindUsImage2']}');
            background-size: cover;
            /* object-fit: cover; */
            @media (max-width: 769px) {
                background-image: url('${images['AboutHowToFindUsImage']}');
                width: 100%;
                min-width:unset;
                height: 272px;
                /* object-fit: cover; */
            }
        }
        & > .card-content {
            padding-top: 48px;
            padding-left: 48px;
            padding-right: 48px;
            padding-bottom: 48px;
            width: 100%;
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                padding-top: 16px;
                padding-left: 16px;
                padding-right: 16px;
                padding-bottom: 16px;
            }
            @media (max-width: 376px) {
            }
            h2 {
                padding: 16px;
            }
            .gmap_canvas {
                overflow: hidden;
                position: relative;
                width: 100%;
                max-width: 700px;
                height: 160px;
            }
            iframe {
                border: 0;
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
            }
            img {
                width: 100%;
                max-width: 584px;
                height: 160px;
                object-fit: cover;
            }
            .address {
                margin-top: 28px;

                h4 {
                    font-size: 24px;
                    line-height: 150%;
                    margin: 0;
                    @media (max-width: 769px) {
                        font-size: 22px;
                    }
                    @media (max-width: 481px) {
                        font-size: 20px;
                    }
                    @media (max-width: 376px) {
                        font-size: 18px;
                    }
                }
                p {
                    font-size: 18px;
                    @media (max-width: 769px) {
                        font-size: 18px;
                    }
                    @media (max-width: 481px) {
                        font-size: 16px;
                    }
                    @media (max-width: 376px) {
                        font-size: 15px;
                    }
                    margin: 0;
                }
            }
        }
    }
`;
const FindUsCard = ({ data: { title, address = {} } }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME, 'flex center')}>
            <div className="img" />
            {/* <img src={images['AboutHowToFindUsImage']} alt="how to find us" /> */}
            <div className="card-content">
                <h2>{title}</h2>
                <div className="mapouter">
                    <div className="gmap_canvas">
                        <iframe
                            width="588"
                            height="160"
                            id="gmap_canvas"
                            src={`https://maps.google.com/maps?q=${encodeURIComponent(
                                address.street
                            )}&t=&z=16&ie=UTF8&iwloc=&output=embed`}
                            frameBorder="0"
                            scrolling="no"
                            marginHeight="0"
                            marginWidth="0"
                        ></iframe>
                    </div>
                </div>
                {/* <img src={images['AboutHowToFindUsMapImage']} alt="address" /> */}
                <div className="address">
                    <h4>{address.city}</h4>
                    <p>{address.street}</p>
                </div>
            </div>
        </Wrapper>
    );
};

FindUsCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string,
        address: PropTypes.shape({
            city: PropTypes.string,
            street: PropTypes.string,
        }),
    }),
};

export default FindUsCard;
