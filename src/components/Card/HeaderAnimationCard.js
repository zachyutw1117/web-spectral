import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import RadianButton from '../Button/RadianButton';
import ChakraHubspotFormDialog from '../Dialog/ChakraHubspotFormDialog';
// import { renderLotties } from '../../config/lottiesBinding';
// import images from '../../config/imagesBinding';
import config from '../../config';
const CARD_CLASS_NAME = 'header-animation-card';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        display: flex;
        flex-direction: row;
        width: 100%;
        align-items: flex-start;
        padding-bottom: 383px;
        padding-top: 50px;

        @media (max-width: 1440px) {
            padding-bottom: 185px;
        }
        @media (max-width: 1024px) {
            padding-bottom: 135px;
        }
        @media (max-width: 769px) {
            padding-top: 0;
            flex-direction: column-reverse;
        }
        @media (max-width: 481px) {
            padding-top: 0;
            padding-bottom: 100px;
        }
        @media (max-width: 376px) {
        }

        .card-image {
            min-width: 100%;
            position: relative;
            flex: 1 1 100%;
            display: flex;
            justify-content: flex-end;
            @media (max-width: 769px) {
                min-height: 461px;
            }
            @media (max-width: 481px) {
                min-height: 345px;
            }
            @media (max-width: 376px) {
            }
            .img-main {
                position: absolute;
                width: 810.58px;
                top: 28px;
                left: 42px;
                max-width: unset;
                @media (max-width: 1440px) {
                    width: 625px;
                    left: -10px;
                }
                @media (max-width: 1024px) {
                    top: 0;
                    left: -85px;
                }
                @media (max-width: 769px) {
                    top: 10px;
                    left: 140px;
                    width: 500px;
                }
                @media (max-width: 481px) {
                    width: 370px;
                    left: 48px;
                }
                @media (max-width: 376px) {
                    left: 5px;
                }
                svg {
                    z-index: 1;
                }
            }
            .img-grid {
                position: absolute;
                top: -150px;
                right: 498px;
                height: 1232.99px;
                max-width: unset;
                /* height: auto; */
                @media (max-width: 1440px) {
                    top: -110px;
                    height: 955px;
                    right: 635px;
                }
                @media (max-width: 1024px) {
                    right: 358px;
                }
                @media (max-width: 769px) {
                    height: 760px;
                    right: 70px;
                }
                @media (max-width: 481px) {
                    right: -120px;
                    top: -250px;
                    height: 1000px;
                }
                @media (max-width: 376px) {
                    right: -180px;
                }
                svg {
                    z-index: 1;
                }
            }
        }
        .card-lottie {
            flex: 1 1 100%;
            display: flex;
            justify-content: flex-end;
            transform: scale(2) translateY(100px) translateX(50px);
            @media (max-width: 1440px) {
                transform: scale(2) translateY(100px) translateX(30px);
            }
            @media (max-width: 1157px) {
                transform: scale(2.2) translateY(80px) translateX(10px);
            }
            @media (max-width: 1060px) {
                transform: scale(2.4) translateY(80px) translateX(0px);
            }
            @media (max-width: 1024px) {
                transform: scale(2.2) translateY(90px) translateX(0px);
            }
            @media (max-width: 940px) {
                transform: scale(2.6) translateY(90px) translateX(0px);
            }
            @media (max-width: 834px) {
                transform: scale(3.5) translateY(60px) translateX(-10px);
            }
            @media (max-width: 769px) {
                transform: scale(1);
            }
            @media (max-width: 481px) {
                transform: scale(1.6) translateY(-40px);
            }
            @media (max-width: 376px) {
            }
            .lottie {
                max-width: 1200px;
                width: 100%;
                height: auto;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    width: 100vw;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
                svg {
                    z-index: 1;
                }
            }
        }

        .card-content {
            /* max-width: 100vw; */
            min-width: 765px;
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            z-index: 1;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                max-width: 510px;
                min-width: 510px;
            }
            @media (max-width: 769px) {
                max-width: 100%;
                min-width: 100%;
                margin-top: -30px;
                padding-left: 48px;
                padding-right: 48px;
            }
            @media (max-width: 481px) {
                padding-left: 35px;
                padding-right: 35px;
                margin-top: -37px;
            }
            @media (max-width: 376px) {
                padding-left: 22px;
                padding-right: 22px;
            }
            h1 {
                z-index: 2;
                width: 100%;
                max-width: 728px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    font-size: 60px;
                    max-width: 412px;
                    /* margin: 40px 0 50px 0; */
                }
                @media (max-width: 769px) {
                    font-size: 56px;
                    margin-top: 0;
                    margin-bottom: 25px;
                    max-width: 640px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    font-size: 48px;
                    max-width: 320px;
                }
            }
            p {
                width: 100%;
                max-width: 641px;

                @media (max-width: 1440px) {
                    max-width: 700px;
                }
                @media (max-width: 1024px) {
                    max-width: 412px;
                }
                @media (max-width: 769px) {
                    max-width: 640px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    font-size: 18px;
                    max-width: 320px;
                }
            }
            .button-group {
                display: flex;
                margin-top: 54px;
                @media (max-width: 1440px) {
                    margin-top: 76px;
                }
                @media (max-width: 1024px) {
                    margin-top: 86px;
                }
                @media (max-width: 769px) {
                    margin-top: 54px;
                }
                @media (max-width: 630px) {
                    flex-direction: column;
                    width: 100%;
                }
                @media (max-width: 481px) {
                    padding-left: 45px;
                    padding-right: 45px;
                    margin-top: 45px;
                }
                @media (max-width: 376px) {
                    padding-left: 14px;
                    padding-right: 14px;
                }
                & > .button:nth-child(even) {
                    margin-left: 24px;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                    }
                    @media (max-width: 630px) {
                        margin-left: 0;
                        margin-top: 24px;
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                    }
                }
                .button {
                    font-size: 20px;
                    /* padding: 14px 54px; */
                    text-align: center;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                    }
                    @media (max-width: 481px) {
                        flex: 1 1;
                        margin-left: 0;
                        width: 100%;
                        max-width: 300px;
                    }
                    @media (max-width: 376px) {
                        padding: 14px 40px;
                    }
                }
            }
        }
    }
`;

const HeaderAnimationCard = ({ data: { title, description, buttonGroup = {}, animation } }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME)}>
            <div className="card-content">
                <h1>{title}</h1>
                <p>{description}</p>
                <div className="button-group">
                    {Object.values(buttonGroup).map(({ text, ...buttonRest }, index) => {
                        if (index === 0) {
                            return (
                                <ChakraHubspotFormDialog
                                    key={index}
                                    trigger={({ onClick }) => (
                                        <RadianButton
                                            key={index}
                                            onClick={onClick}
                                            variant={index % 2 === 0 ? 'solid' : 'outline'}
                                        >
                                            {text}
                                        </RadianButton>
                                    )}
                                    iframeSrc={buttonRest.href}
                                />
                            );
                        } else {
                            return (
                                <RadianButton
                                    key={index}
                                    variant={index % 2 === 0 ? 'solid' : 'outline'}
                                    {...buttonRest}
                                >
                                    {text}
                                </RadianButton>
                            );
                        }
                    })}
                </div>
            </div>
            <div className="card-image">
                {animation && (
                    <>
                        <img
                            className="img-grid"
                            src={config.images['HomeS01Grid']}
                            alt="s01"
                            // style={{ width: '100%', height: '100%' }}
                        />
                        <img
                            className="img-main"
                            src={config.svgs['HomeS01Main']}
                            alt="s01"
                            // style={{ width: '100%', height: '100%' }}
                        />
                    </>
                )}
                {/* renderLotties[animation] */}
            </div>
            {/* <div className="card-lottie">
                {
                  
                    renderLotties[animation]
                }
            </div> */}
        </Wrapper>
    );
};

HeaderAnimationCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string,
        description: PropTypes.string,
        buttonGroup: PropTypes.object,
        animation: PropTypes.any,
    }),
};

export default HeaderAnimationCard;
