import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import config from '../../config';

const TOTAL_NUMBER_OF_STACK_CARD = 6;
const Wrapper = styled.div`
    background: var(--text-light);
    max-width: 400px;
    min-height: 448px;
    width: 100%;
    padding: 60px 40px 30px 40px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    overflow: hidden;
    box-shadow: 0px 8px 40px rgba(182, 174, 197, 0.24);
    border-radius: 8px;
    .title {
        font-family: Inter;
        font-style: normal;
        font-weight: bold;
        font-size: 24px;
        line-height: 36px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            font-size: 22px;
            margin: 8px 0;
        }
        @media (max-width: 481px) {
            font-size: 20px;
        }
        @media (max-width: 376px) {
            font-size: 18px;
        }
    }
    p.description {
        font-family: 'Inter';
        font-style: normal;
        font-weight: normal;
        font-size: 18px;
        line-height: 27px;
        /* or 24px */
        letter-spacing: 0.04em;
        margin: 0;
    }

    img {
        width: 70px;
    }
    .rectangle {
        margin: 18px 0;
        border-radius: 6px;
    }
    a {
        color: var(--text-light);
        font-family: Inter;
        font-style: normal;
        font-weight: 500;
        font-size: 14px;
        line-height: 150%;
        /* identical to box height, or 21px */
        text-align: right;
        letter-spacing: 0.04em;
        text-decoration-line: underline;
    }
`;

const FeatureStackCard = ({ data: { title, description, imageSrc, name }, index, className }) => {
    return (
        <Wrapper className={className} id={`feature-stack-card-${name}`}>
            <img className="news-icon" src={config.svgs[imageSrc]} alt="source" />
            <h4 className="title">{title}</h4>
            <span
                className="rectangle"
                style={{
                    height: '5px',
                    width: '80px',
                    backgroundColor: `var(--feature-stack-color-${
                        index % TOTAL_NUMBER_OF_STACK_CARD
                    })`,
                    boxShadow: `0px 0px 15px var(--feature-stack-color-${
                        index % TOTAL_NUMBER_OF_STACK_CARD
                    })`,
                }}
            />
            <p className="description"> {description}</p>
        </Wrapper>
    );
};

FeatureStackCard.propTypes = {
    className: PropTypes.string,
    data: PropTypes.object.isRequired,
    classNames: PropTypes.string,
    index: PropTypes.number,
};

export default FeatureStackCard;
