import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import svgs from '../../config/svgsBinding';
import docs from '../../config/docsBinding';
import { cookieBanner } from '../../config/content/footer.json';
import RadianButton from '../Button/RadianButton';
const Wrapper = styled.div`
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    z-index: 5000;
    display: flex;
    justify-content: center;
    height: auto;

    & > .card_content {
        position:relative;
        padding:24px;
        width:100%;
        display:flex;
        flex-direction:row;
        justify-content:center;
        align-items:center;
        color:var(--text-light);
        background-image:url('${svgs[cookieBanner.bg]}');
        background-size:cover;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            padding:12px;
        }
        @media (max-width: 769px) {
            flex-direction:column-reverse;
        }
        @media (max-width: 481px) {
            padding:12px;
        }
        @media (max-width: 376px) {
            padding:12px 12px 12px 12px;
            
        }
        .text-area{
            padding:0 28px;
            width:700px;
            font-family: 'Inter';
            font-style: normal;
            font-weight: normal;
            font-size: 20px;
            line-height: 176%;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                width:80%;
                padding:0 14px;
                font-size: 18px;
                margin: 8px 0;
            }
            @media (max-width: 481px) {
                width:100%;
                
            }
            @media (max-width: 376px) {
                font-size: 16px;
            }
        }
        h4{
            font-size:28px;
            font-weight:400px;
            letter-spacing: 0.025em;
            margin:0;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                font-size:22px;
                
            }
            @media (max-width: 769px) {
                font-size:20px;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
                font-size:18px;
            }
        }
        p {
            margin:0;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                font-size:18px;
            }
            @media (max-width: 769px) {
                font-size:16px;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
                font-size:14px;
            }
        }
        a {
            color:var(--text-light);
            text-decoration:underline;
            font-weight:500;
            cursor: pointer;
        }
        .close_icon {
            position:absolute;
            left:15px;
            top:15px;
            height:32px;
            width:32px;
            cursor: pointer;
            z-index:8888;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                left:7px;
                top:7px;
            }
            @media (max-width: 376px) {
            }
        }
        .bg {
            position:absolute;
            top:0;
            left:0;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                height: 50px;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
            width: 100%;
            object-fit: cover;
        }
    }
`;
const CookieBannerCard = ({ onClose, onAccept, isOpen }) => {
    return isOpen ? (
        <Wrapper>
            <div className="card_content">
                <div className="text-area">
                    <b>{cookieBanner.highLight}</b>
                    <span>{cookieBanner.description}</span>
                    <a href={docs[cookieBanner.doc.href]}>{cookieBanner.doc.text}</a>
                </div>
                <RadianButton onClick={onAccept}>Accept</RadianButton>
                <img className="close_icon" src={svgs['XIcon']} alt="x" onClick={onClose} />
            </div>
        </Wrapper>
    ) : null;
};

CookieBannerCard.propTypes = {
    onClose: PropTypes.func,
    onAccept: PropTypes.func,
    isOpen: PropTypes.bool,
};

export default CookieBannerCard;
