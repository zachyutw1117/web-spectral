import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import config from '../../config';

const CARD_CLASS_NAME = 'promotion-card';

const PromotionCardWrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        text-align: center;
        justify-content: center;
        align-items: center;
        margin: 0 0 70px 0;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
            margin: 0 0 55px 0;
        }

        .image-wrapper {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            display: flex;
            align-items: flex-start;
            z-index: 0;
            img.promotion-image {
                width: 200.5px;
            }
        }
        .card-content {
            padding-top: 210px;
            z-index: 1;
            max-width: 320px;
            @media (max-width: 1440px) {
                padding-top: 210px;
            }
            @media (max-width: 1024px) {
                max-width: 280px;
            }
            @media (max-width: 769px) {
                max-width: 432px;
            }
            @media (max-width: 481px) {
                max-width: 320px;
            }
            @media (max-width: 376px) {
            }
            h4 {
                font-size: 24px;
                padding: 0 10px;
                margin-bottom: 10px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1224px) {
                }
                @media (max-width: 1165px) {
                    /* min-height: 110px; */
                }
                @media (max-width: 1024px) {
                    line-height: 36px;
                    padding: 0 3px;
                }
                @media (max-width: 957px) {
                    /* min-height: 145px; */
                }
                @media (max-width: 769px) {
                    font-size: 22px;
                    min-height: unset;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                }
            }
            p {
                margin: 0;
                padding: 0 10px;
                font-size: 18px;
                line-height: 150%;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    font-size: 18px;
                    padding: 0 5px;
                }
                @media (max-width: 769px) {
                    padding: 0;
                    font-size: 18px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    font-size: 16px;
                    max-width: 320px;
                }
            }
        }
    }
`;
const PromotionCard = ({ data: { title, description }, index, className, ...rest }) => {
    return (
        <PromotionCardWrapper
            className={clsx(CARD_CLASS_NAME, className, {
                [`index${index + 1}`]: true,
            })}
            {...rest}
        >
            <div className="image-wrapper flex center">
                <img
                    className="promotion-image"
                    src={config.svgs[`PromotionImage${index + 1}`]}
                    alt="promotion"
                />
            </div>

            <div className="card-content">
                <h4>{title}</h4>
                <p title={description}>{description}</p>
            </div>
        </PromotionCardWrapper>
    );
};

PromotionCard.propTypes = {
    data: PropTypes.object.isRequired,
    className: PropTypes.string,
    index: PropTypes.number,
};

export default PromotionCard;
