import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import images from '../../config/imagesBinding';

const CARD_CLASS_NAME = 'employee-card';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        position: relative;
        text-align: center;
        flex-direction: column;
        width: 100%;
        max-width: 344px;
        height: 379px;
        background: #ffffff;
        box-shadow: 0px 8px 40px rgba(182, 174, 197, 0.24);
        border-radius: 8px;
        overflow: hidden;
        margin: 20px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            max-width: 280px;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        img {
            width: 100%;
            height: 260px;
            object-fit: cover;
            z-index: 0;
        }
        .card-content {
            position: absolute;
            top: 260px;
            padding: 24px;
            left: 0;
            width: 100%;
            background: #ffffff;
            z-index: 1;

            height: 379px;
            overflow-y: auto;
            transition: top 600ms ease-in-out;
        }
        h4 {
            font-family: Inter;
            font-style: normal;
            font-weight: bold;
            font-size: 24px;
            line-height: 150%;
            text-align: center;
            margin: 0;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                font-size: 22px;
            }
            @media (max-width: 481px) {
                font-size: 20px;
            }
            @media (max-width: 376px) {
            }
        }
        b {
            font-family: Inter;
            font-style: normal;
            font-weight: normal;
            font-size: 18px;
            line-height: 150%;
            /* identical to box height, or 27px */
            text-align: center;
            letter-spacing: 0.04em;
            color: #7535fd;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                font-size: 16px;
            }
            @media (max-width: 376px) {
            }
        }
        p {
            opacity: 0;
            font-size: 15px;
            text-align: center;
            transition: opacity 600ms ease-in-out;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                font-size: 15px;
            }
            @media (max-width: 376px) {
            }
        }
        &:hover .card-content {
            top: 0;
            transition: top 1s ease-in-out;
        }
        &:hover p {
            opacity: 1;
            margin-top: 8px;
            transition: opacity 1s ease-in-out;
        }
    }
`;

const EmployeeCard = ({ data: { title, text, imageSrc, description } }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME)}>
            <img src={imageSrc ? imageSrc : images['AboutEmployeeCardBgImage']} alt="background" />
            <div className="card-content hide-scrollbar">
                <h4>{title}</h4>
                <b>{text}</b>
                <p>{description}</p>
            </div>
        </Wrapper>
    );
};

EmployeeCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string,
        text: PropTypes.string,
        imageSrc: PropTypes.string,
        description: PropTypes.string,
    }),
};

export default EmployeeCard;
