import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';

const CARD_CLASS_NAME = 'characteristic-card';
const titleColor = index => `var(--characteristic-color-${index})`;

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        flex-direction: column;
        padding: 25px;
        h2 {
            margin: 0;
        }
        p {
            margin: 0;
            @media (max-width: 376px) {
                font-size: 16px;
            }
        }
    }
`;
const CharacteristicCard = ({ data: { title, text }, index = 0 }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME, 'flex center')}>
            <h2 style={{ color: titleColor(index) }}>{title}</h2>
            <p>{text}</p>
        </Wrapper>
    );
};

CharacteristicCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string,
        text: PropTypes.string,
    }),
    index: PropTypes.number,
};

export default CharacteristicCard;
