import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import svgs from '../../config/svgsBinding';
import RadianButton from '../Button/RadianButton';
import ChakraHubspotFormDialog from '../Dialog/ChakraHubspotFormDialog';

const CARD_CLASS_NAME = 'fox-chart-banner-card';

const Wrapper = styled.div`
    &.${CARD_CLASS_NAME} {
        position: relative;
        width: 100%;
        display: flex;
        max-width: 1925px;
        background: var(--purple);
        overflow: hidden;
        box-shadow: 0px 8px 40px var(--purple-shadow);
        border-radius: 16px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            flex-direction: column;
            margin: 0;
            max-height: 825px;
        }
        @media (max-width: 481px) {
            border-radius: 0;
        }
        @media (max-width: 376px) {
            max-height: 776px;
        }
        /* filter: drop-shadow(0px 8px 40px rgba(117, 53, 253, 0.32)); */
        img.chart-bottom {
            position: absolute;
            width: 100%;
            bottom: 0;
            left: 0;
            @media (max-width: 1024px) {
                width: 100%;
                object-fit: cover;
                height: 550px;
            }
            @media (max-width: 769px) {
                height: 797px;
            }
        }

        .foxChart-content {
            position: relative;
            z-index: 2;
            width: 100%;
            max-width: 472px;
            color: var(--text-light);
            margin-top: 96px;
            margin-left: 84px;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                margin-left: 32px;
            }
            @media (max-width: 769px) {
                margin-left: 64px;
                margin-top: 50px;
                max-width: calc(100% - 64px);
            }
            @media (max-width: 481px) {
                margin-left: 24px;
                max-width: calc(100% - 24px);
            }
            @media (max-width: 376px) {
                margin-left: 30px;
                max-width: calc(100% - 14px);
            }
            h2 {
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    width: 400px;
                }
                @media (max-width: 769px) {
                    width: 350px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
            }
            p {
                width: 441px;
                font-size: 24px;
                margin: 24px 0 24px 0;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    font-size: 22px;
                    width: 400px;
                }
                @media (max-width: 769px) {
                    font-size: 20px;
                    width: 350px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
            }

            .button {
                z-index: 5;
                margin-top: 40px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    margin-top: 30px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
            }
            & > img.arrow {
                position: absolute;
                top: 220px;
                left: 240px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    top: 190px;
                }
                @media (max-width: 769px) {
                    top: 170px;
                }
                @media (max-width: 650px) {
                }
                @media (max-width: 481px) {
                    /* top:220px; */
                }
                @media (max-width: 430px) {
                    /* top: 250px; */
                    display: none;
                }
                @media (max-width: 376px) {
                }
            }
        }
        .foxChart-chart {
            position: relative;
            z-index: 1;
            flex: 1 1;
            height: 560px;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                height: 500px;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
            & > img.fox-image {
                position: absolute;
                left: -50px;
                top: -165px;
                width: 926.17px;
                height: 880.68px;
                max-width: unset;
                z-index: 2;
                /* filter: drop-shadow(0px 0px 50px #e6defe); */
                transform: scale(1);
                @media (max-width: 1440px) {
                    left: -130px;
                }
                @media (max-width: 1024px) {
                    transform: scale(1);
                    left: -205px;
                    top: -130px;
                }
                @media (max-width: 769px) {
                    display:none;
                    top: unset;
                    bottom: -190px;
                    left: -30px;
                    transform: scale(0.88);
                }
                @media (max-width: 481px) {
                    /* top: -220px; */
                    left: -200px;
                    transform: scale(0.7);
                }
                @media (max-width: 376px) {
                    bottom: -135px;
                    transform: scale(0.72);
                }
            }

            & > img.fox-mobile-image {
                position: absolute;
                max-width: unset;
                display: none;
                z-index: 2;
                width: 678px;
                /* height: 760px; */
                @media (max-width: 769px) {
                    display: block;
                    right: 0;
                    top: -280px;
                }
                @media (max-width: 481px) {
                    /* width: 578px; */
                    right: unset;
                    left: -90px;
                    /* top: -220px; */

                    /* transform: matrix(0.93, 0.36, -0.36, 0.93, 0, 0); */
                    /* top: 0; */
                }
                @media (max-width: 376px) {
                    top: -350px;
                    left: -85px;
                }
            }
            & > .fox-mobile-grid-image {
                position: absolute;
                background-image:url('${svgs['HomeFoxChartFoxGridImage']}');
                background-size: 539px auto;
                min-width: 539px;
                min-height: 534px;
                max-width: unset;
                display: none;
                z-index: 2;
                
                /* object-fit: contain; */
                /* height: 760px; */
                @media (max-width: 769px) {
                    display: block;
                    right: 70px;
                    bottom: 50px;
                }
                @media (max-width: 481px) {
                    /* top: -220px; */
                    background-image:url('${svgs['HomeFoxChartFoxGrid481Image']}');
                    background-size: 343px auto;
                    min-width: 343px;
                    min-height: 534px;
                    right: 35px;
                    /* transform: matrix(0.93, 0.36, -0.36, 0.93, 0, 0); */
                    /* top: 0; */
                }
                @media (max-width: 376px) {
                    background-image:url('${svgs['HomeFoxChartFoxGrid375Image']}');
                    background-size: 281px auto;
                    min-width: 281px;
                    min-height: 534px;
                    right: 15px;
                    bottom: 0;
                }
            }
            .columns {
                display: flex;
                justify-content: space-evenly;
                height: 100%;
                z-index: 1;
            }
        }
    }
`;

const FoxColumnWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    b {
        flex: 1 1;
        width: 2px;
        background-image:url('${svgs['HomeFoxChartColumnLineImage']}');
        background-repeat: repeat-y;
        /* border-left: 4px dashed #fff; */
        opacity: 0.8;
    }
    p{
        color:var(--text-light)
    }

`;
const FoxColumn = ({ column }) => {
    return (
        <FoxColumnWrapper>
            <b />
            <p>{column}</p>
        </FoxColumnWrapper>
    );
};
FoxColumn.propTypes = {
    column: PropTypes.string,
};
const FoxChartBannerCard = ({ data: { title, description, buttonText } }) => {
    return (
        <Wrapper className={clsx(CARD_CLASS_NAME)}>
            <img
                className="chart-bottom"
                src={svgs['HomeFoxChartPurpleChart']}
                alt="chart-bottom"
            />
            <div className="foxChart-content">
                <h2>{title}</h2>
                <p>{description}</p>
                <img className="arrow" src={svgs['HomeFoxChartArrowImage']} alt="arrow" />
                <ChakraHubspotFormDialog
                    trigger={dialogProp => (
                        <RadianButton variant="outline" color="white" {...dialogProp}>
                            {buttonText}
                        </RadianButton>
                    )}
                />
            </div>
            <div className="foxChart-chart">
                <img className="fox-image" src={svgs['HomeFoxChartFoxImage']} alt="fox" />
                <div className="fox-mobile-grid-image" />
                {/* <img
                    className="fox-mobile-grid-image"
                    src={svgs['HomeFoxChartFoxGridImage']}
                    alt="fox"
                /> */}
                <img
                    className="fox-mobile-image"
                    src={svgs['HomeFoxChartFoxMobileImage']}
                    alt="fox"
                />

                <div className="columns">
                    {/* {columns.map(column => (
                        <FoxColumn column={column} key={column} />
                    ))} */}
                </div>
            </div>
        </Wrapper>
    );
};

FoxChartBannerCard.propTypes = {
    data: PropTypes.shape({
        href: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        buttonText: PropTypes.string,
        columns: PropTypes.array,
    }),
};

export default FoxChartBannerCard;
