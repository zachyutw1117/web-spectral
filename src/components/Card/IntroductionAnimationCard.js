import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { renderLotties } from '../../config/lottiesBinding';
import RadianButton from '../Button/RadianButton';
import svgs from '../../config/svgsBinding';
const CARD_ID = 'home-introduction-card';

const Wrapper = styled.div`
    &.${CARD_ID} {
        display: flex;
        flex-direction: row;
        width: 100%;
        align-items: flex-start;
        padding-top: 160px;
        padding-bottom: 160px;
        &.reverse {
            flex-direction: row-reverse;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                padding-bottom: 102px;
                flex-direction: column-reverse;
                text-align: center;
                justify-content: center;
                padding-left: 0;
                padding-right: 0;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
        }
        @media (max-width: 1440px) {
        }

        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            padding-bottom: 102px;
            flex-direction: column-reverse;
            text-align: center;
            justify-content: center;
            padding-left: 0;
            padding-right: 0;
        }
        @media (max-width: 481px) {
            padding-top: 120px;
            padding-bottom: 120px;
        }
        @media (max-width: 376px) {
            padding-top: 100px;
        }
        .card-lottie {
            flex: 1 1 100%;
            display: flex;
            justify-content: flex-end;
            margin: 0 80px;
            margin: 0 55px;
            @media (max-width: 1440px) {
                margin: 0;
            }
            @media (max-width: 1200px) {
                justify-content: center;
                margin: 0;
                width: 100%;
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
            .lottie {
                width: 620px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1200px) {
                    width: 511px;
                }
                @media (max-width: 1024px) {
                    max-width: 511px;
                    width: 100%;
                }
                @media (max-width: 769px) {
                }
                @media (max-width: 481px) {
                    max-width: 400px;
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                }
            }
        }
        .card-image {
            flex: 1 1 100%;
            display: flex;
            justify-content: flex-end;
            margin: 0 80px;
            @media (max-width: 1440px) {
                margin: 0;
            }
            @media (max-width: 1200px) {
                justify-content: center;
                margin: 0;
                width: 100%;
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
            img {
                width: 620px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1200px) {
                    width: 511px;
                }
                @media (max-width: 1024px) {
                    max-width: 454px;
                    width: 100%;
                }
                @media (max-width: 769px) {
                    max-width: 400px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                }
            }
        }
        &.reverse .card-lottie,
        &.reverse .card-image {
            justify-content: flex-start;
            @media (max-width: 1440px) {
            }
            @media (max-width: 769px) {
                justify-content: center;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
        }
        .card-lottie {
            @media (max-width: 1440px) {
            }
            @media (max-width: 1200px) {
                justify-content: center;
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
        }

        .card-content {
            width: 100%;
            max-width: 440px;
            margin: 20px 80px;
            @media (max-width: 1440px) {
                margin: 20px 0;
            }
            @media (max-width: 1024px) {
                max-width: 400px;
                margin: 20px 14px;
            }

            @media (max-width: 769px) {
                margin: 50px 0;
                max-width: 100%;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                padding-left: 0;
                padding-right: 0;
            }
            @media (max-width: 481px) {
                margin: 74px 0 50px 0;
            }
            @media (max-width: 376px) {
            }
            h2 {
                color: #11213f;
                @media (max-width: 769px) {
                    width: 100%;
                    max-width: 520px;
                }
                @media (max-width: 481px) {
                    max-width: 400px;
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                }
            }
            p {
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    font-size: 22px;
                }
                @media (max-width: 769px) {
                    font-size: 20px;
                    width: 100%;
                    max-width: 640px;
                }
                @media (max-width: 481px) {
                    max-width: 400px;
                    font-weight: 500;
                }
                @media (max-width: 376px) {
                    font-size: 18px;
                    max-width: 330px;
                }
            }
            .button {
                padding: 14px 32px !important;
                font-size: 16px !important;
                font-weight: bold;
                margin-top: 32px;
                font-family: Inter;
                font-style: normal;

                line-height: 110%;
                /* identical to box height, or 18px */
                align-items: center;
                letter-spacing: 0.02em;
                text-transform: capitalize;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    /* margin-top: 17px; */
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    font-size: 14px !important;
                }
            }
        }
    }
`;

const IntroductionAnimationCard = ({
    isSvg,
    data: { animation, title, description, buttonText, href, to },
    reverse,
    className,
}) => {
    return (
        <Wrapper className={clsx(CARD_ID, className, { reverse })}>
            <div className="card-content">
                <h2>{title}</h2>
                <p>{description}</p>
                {buttonText && (
                    <RadianButton href={href} to={to} variant="solid">
                        {buttonText}
                    </RadianButton>
                )}
            </div>

            {isSvg ? (
                <div className="card-image">
                    <img src={svgs[animation]} alt="intro" />
                </div>
            ) : (
                <div className="card-lottie">{renderLotties[animation]}</div>
            )}
        </Wrapper>
    );
};

IntroductionAnimationCard.propTypes = {
    data: PropTypes.shape({
        animation: PropTypes.any,
        title: PropTypes.string,
        description: PropTypes.string,
        buttonText: PropTypes.string,
        href: PropTypes.string,
        to: PropTypes.string,
    }),
    reverse: PropTypes.bool,
    isSvg: PropTypes.bool,
    className: PropTypes.string,
};

export default IntroductionAnimationCard;
