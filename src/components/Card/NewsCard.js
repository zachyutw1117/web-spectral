import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import config from '../../config';
const CARD_CLASS_NAME = 'news-card';

// const TOTAL_NUMBER_OF_NEWS_CARD = 6;
const Wrapper = styled.a`
    &.${CARD_CLASS_NAME} {
        width: 258px;
        height: 312px;
        margin: 24px 12px;
        background: var(--text-light);
        color: var(--text-link);
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        /* padding: 1rem; */
        border-radius: 8px;
        overflow: hidden;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            width: calc(280px - 20px);
        }
        @media (max-width: 769px) {
            width: 100%;
            max-width: 280px;
            height: auto;
        }
        @media (max-width: 481px) {
            width: calc(100% - 20px);
            justify-content: flex-start;
            height: auto;
        }
        @media (max-width: 376px) {
        }
        h4 {
            font-style: normal;
            font-weight: bold;
            font-size: 24px;
            line-height: 176%;
            margin: 0;
        }
        .date {
            font-family: Inter;
            font-style: normal;
            font-weight: normal;
            font-size: 12px;
            line-height: 150%;
            margin-bottom: 5px;
            /* identical to box height, or 18px */

            letter-spacing: 0.04em;

            /* gray lihgt text */

            color: #7a8394;
        }
        p {
            font-family: 'Inter';
            font-style: normal;
            font-weight: bold;
            font-size: 16px;
            line-height: 150%;
            /* or 24px */
            letter-spacing: 0.04em;
            color: #122d5f;
            margin-bottom: 20px;
        }

        img {
            height: 128px;
            object-fit: cover;
        }
        .card-content {
            padding: 20px;
        }
        .description {
            position: relative;
            max-height: 75px;
            height: 75px;
            overflow: hidden;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            display: -webkit-box;
        }
        a {
            color: var(--text-link);

            text-decoration-line: underline;
        }
        .news-source-text {
            color: #7e7d7d;
            font-weight: 800;
            text-transform: uppercase;
        }
        .news-icon {
            width: 16px;
            height: 16px;
            margin-right: 5px;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
                display: none;
            }
            @media (max-width: 376px) {
            }
        }
    }
`;

const NewsCard = ({
    data: { title, description, name, source = {}, imageSrc, postDate },
    // index = 0,
    className,
}) => {
    return (
        <Wrapper
            href={source.href}
            target="_blank"
            rel="noreferrer"
            className={clsx(CARD_CLASS_NAME, className)}
            data-testid={`news-card-${name}`}
        >
            <img src={imageSrc} alt={title} />
            <div className="card-content">
                {/* <div className="news-card-title" title={title}>
                        <a href={source.href} target="_blank" rel="noreferrer">
                            <h4>{title}</h4>
                        </a>
                    
                    </div> */}
                <div className="date">{postDate}</div>

                <p className="description"> {`${description}`} </p>

                <div className="flex">
                    {source.icon && (
                        <img className="news-icon" src={config.images[source.icon]} alt="source" />
                    )}
                    {source.text && <span className="news-source-text">{source.text}</span>}
                    {/* <span>read more</span> */}
                </div>
            </div>
        </Wrapper>
    );
};

NewsCard.propTypes = {
    className: PropTypes.string,
    data: PropTypes.shape({
        title: PropTypes.string,
        description: PropTypes.string,
        name: PropTypes.string,
        postDate: PropTypes.string,
        source: PropTypes.object,
        imageSrc: PropTypes.string,
    }),
    classNames: PropTypes.string,
    index: PropTypes.number,
};

export default NewsCard;
