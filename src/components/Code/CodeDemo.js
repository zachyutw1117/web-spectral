import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import SyntaxHighlighter from 'react-syntax-highlighter';
import clsx from 'clsx';
import { Select, ThemeProvider, theme } from '@chakra-ui/core';
import { isblEditorDark } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import PropTypes from 'prop-types';
import svgs from '../../config/svgsBinding';
import config from '../../config';

const CodeDemoWrapper = styled.div`
    width: 90%;
    max-width: 1000px;
    display: inline-flex;
    position: relative;
    text-align: left;
    box-shadow: 0px 8px 180px var(--blue-shadow);
    border-radius: 8px;

    @media (max-width: 1440px) {
    }
    @media (max-width: 1024px) {
    }
    @media (max-width: 769px) {
        flex-direction: column-reverse;
    }
    @media (max-width: 481px) {
    }
    @media (max-width: 376px) {
    }
    pre {
        flex-grow: 1;
        margin: 0;
        background: var(--blue5) !important;
    }
    & > pre {
        padding: 40px !important;
        @media (max-width: 769px) {
            padding: 1rem !important;
        }
        code {
            @media (max-width: 769px) {
                font-size: 14px;
            }
        }
    }
    .select-wrapper {
        display: none !important;
        &.DockerIconImage{
            position: relative;
            &:before {
                position: absolute;
                top: 8px;
                left: 15px;
                width: 50px;
                min-width: 50px;
                background-image: url('${svgs['DockerIconImage']}');    
                background-repeat:no-repeat;
                height: 100%;
                display: inline-flex;
                z-index:1;
                content: '';
            }
            select {
                padding-left: 50px;
            }
        }
        &.KubernetesIconImage {
            position: relative;
            &:before {
                position: absolute;
                top: 8px;
                left: 15px;
                width: 50px;
                min-width: 50px;
                background-image: url('${svgs['KubernetesIconImage']}');    
                background-repeat:no-repeat;
                height: 100%;
                display: inline-flex;
                z-index:1;
                content: '';
            }
            select {
                padding-left: 50px;
            }
        }
    
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            display: flex !important;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
    }
    .MuiListItemText-root {
        text-transform: capitalize;
        cursor: pointer;
    }
    .list {
        padding-top: 0;
        padding-bottom: 0;
        background: var(--text-link);
        margin: 0;
        position: relative;
        padding: 0;
        list-style: none;
        font-size: 18px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            display: none;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        & > * {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            position: relative;
            padding-left: 40px;
            padding-right: 40px;
            padding-top: 14px;
            padding-bottom: 14px;
            font-family: 'Inter';
            height: 56px;
            font-style: normal;
            font-weight: 500;
            font-size: 18px;
            line-height: 150%;
            width: 100%;
            background: #122d5f;
            color: #fff;
            /* identical to box height, or 27px */
            border: none;
            letter-spacing: 0.04em;
            text-transform: capitalize;
            @media (max-width: 769px) {
                min-width: 180px;
                padding-right: 14px;
                font-size: 14px;
            }
            svg {
                transform: scaleY(1.02) translateY(-2px);
                @media (max-width: 1024px) {
                    transform: scaleY(1.25);
                }
            }

            .demo-button-bg {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                display: none;
                z-index: 1;
                transition: all 200ms ease-in-out;
            }
            .content {
                z-index: 2;
            }
            cursor: pointer;
            &:hover {
                color: var(--gray-shadow);
            }
            img {
                margin-right: 8px;
                width: 24px;
                height: 24px;
            }
            span {
                text-overflow: ellipsis;
                /* Required for text-overflow to do anything */
                white-space: nowrap;
                overflow: hidden;
            }
        }
        [role='selected'] {
            color: var(--text-link);
            /* background: var(--text-light) !important; */
            .demo-button-bg {
                display: inline-block;
                z-index: 1;
                transition: all 200ms ease-in-out;
                transform: scale(1.12, 1.3) translateX(-10px) translateY(5px);
                @media (max-width: 769px) {
                    transform: scale(1.12, 1.3) translateX(0) translateY(0);
                }
                svg {
                    width: 100%;
                }
            }
        }
    }
`;

const SelectWrapper = styled(Select)`
    &.code-demo-select {
        display: none !important;
        background-color: #122d5f !important;
        color: #fff !important;
        border: none;
        border-radius: 0 !important;
        svg {
            fill: #fff;
        }
        & * {
            color: #fff !important;
        }
        select {
            text-transform: capitalize;
            padding-left: 50px !important;
        }
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            display: flex !important;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
    }
`;

const CodeDemo = ({ codeDemo, ...rest }) => {
    const list = useMemo(() => Object.values(codeDemo), [codeDemo]);
    const [selected, setSelected] = useState(list.length > 0 ? list[0].name : null);
    const selectedCodeDemo = codeDemo[selected] || {};
    return (
        <CodeDemoWrapper {...rest}>
            <ThemeProvider theme={theme}>
                <SyntaxHighlighter language={selectedCodeDemo.language} style={isblEditorDark}>
                    {selectedCodeDemo.code}
                </SyntaxHighlighter>

                <SelectWrapper
                    rootProps={{
                        className: clsx('select-wrapper', {
                            [codeDemo[selected].icon]: codeDemo[selected].icon,
                        }),
                    }}
                    className="code-demo-select"
                    // placeholder="Select option"
                    onChange={e => setSelected(e.target.value)}
                    value={selected}
                >
                    {list.map(({ title, name, icon }, index) => (
                        <option
                            className={clsx({ [icon]: icon })}
                            key={index}
                            value={name}
                            icon={icon}
                        >
                            {title}
                        </option>
                    ))}
                </SelectWrapper>
                <div className="list">
                    {list.map(({ title, name, icon }, index) => (
                        <button
                            title={title}
                            key={name}
                            role={name === selected ? 'selected' : 'unselected'}
                            tabIndex={0}
                            onClick={() => setSelected(name)}
                            data-testid={`demo-button-${index}`}
                        >
                            <div className="demo-button-bg">
                                <svg
                                    width="238"
                                    height="50"
                                    viewBox="0 0 238 50"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M21.2426 1.75736C22.3679 0.632141 23.894 0 25.4853 0H232C235.314 0 238 2.68629 238 6V44C238 47.3137 235.314 50 232 50H25.4853C23.894 50 22.3679 49.3679 21.2426 48.2426L2.24264 29.2426C-0.100505 26.8995 -0.100505 23.1005 2.24264 20.7574L21.2426 1.75736Z"
                                        fill="white"
                                    />
                                </svg>
                            </div>
                            <div className="content flex center">
                                <img alt={icon} src={config.svgs[icon]} height={20} width={20} />
                                <span>{title}</span>
                            </div>
                        </button>
                    ))}
                </div>
            </ThemeProvider>
        </CodeDemoWrapper>
    );
};

CodeDemo.propTypes = {
    codeDemo: PropTypes.object,
};

export default CodeDemo;
