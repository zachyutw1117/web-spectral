import React, { useMemo, useState, useEffect } from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';

// import HeaderDrawer from './HeaderDrawer';
// import RadianButton from '../Button/RadianButton';
import SvgIconButton from '../Button/SvgIconButton';

import config from '../../config';
import svgs from '../../config/svgsBinding';

const headerNav = config.settings.nav;
const headerLogoSrc = svgs['HeaderLogoImage'];

const HeaderWrapper = styled.div`
    position: relative;
    &.isOpen {
        .container {
            background: var(--text-light);
            z-index: 100;
        }
    }
    .header-menu-button {
        display: none;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            display: block;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
    }
    .container {
        position: relative;
        padding-top: 32px;
        padding-bottom: 20px;
        z-index: 5;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            padding: 20px 14px;
        }
        @media (max-width: 376px) {
        }
    }
    #logo {
        width: 150px;
        z-index: 2;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            margin: 0 14px;
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        img {
            object-fit: cover;
            height: 100%;
            width: 100%;
            margin: 0;
        }
    }
    .header-menu {
        position: absolute;
        display: block;
        width: 100%;
        height: auto;
        top: 100%;
        left: 0;
        z-index: 100;
        opacity: 0;
        /* transition: all 200ms ease-in-out; */
        transform: translateY(-300vh);
        &.isOpen {
            display: block;
            opacity: 1;
            /* transition: all 300ms ease-in-out; */
            transform: translateY(0);
            height: 300vh;
        }
        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 100;
            background: rgba(80, 80, 80, 0.5);
        }
        .header-menu-column-list {
            position: relative;
            display: flex;
            flex-direction: column;
            padding: 20px;
            background-color: #fff;
            z-index: 105;
            a,
            button {
                margin: 23px 20px;
                font-family: Inter;
                font-style: normal;
                font-weight: 600;
                font-size: 14px;
                line-height: 110%;
                /* or 15px */

                letter-spacing: 0.02em;
                text-transform: uppercase;
            }
        }
    }

    .header-nav {
        z-index: 2;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            display: none;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .header-section-link {
            padding: 0 24px;
            font-family: Inter;
            font-style: normal;
            font-weight: 600;
            font-size: 14px;
            line-height: 110%;
            /* or 15px */
            letter-spacing: 0.02em;
            text-transform: uppercase;
            & > a {
                color: var(--text-link);
                text-decoration: none;
            }
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                padding: 0 14px;
            }
            @media (max-width: 834px) {
                padding: 0 8px;
            }
            @media (max-width: 769px) {
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
        }
    }
    .header-sign-up-button {
        padding-left: 32px;
        @media (max-width: 769px) {
            padding-left: 14px;
        }
    }
`;

const Header = ({ logoSrc = headerLogoSrc, nav = headerNav }) => {
    const [isOpen, setIsOpen] = useState(false);
    const links = useMemo(
        () =>
            Object.values(nav).map(({ name }) => ({
                text: name,
                to: `/${name}`,
            })),
        [nav]
    );
    useEffect(() => {
        document.body.style.overflow = isOpen ? 'hidden' : 'unset';
    }, [isOpen]);
    return (
        <HeaderWrapper className={clsx('flex center', { isOpen })}>
            <div className="container flex between">
                <div id="logo">
                    <Link to="/">
                        <img alt="logo" src={logoSrc} width="100%" height="100%" />
                    </Link>
                </div>
                <div className="header-nav flex evenly">
                    <div className="flex evenly" id="header-links" data-testid="header-links">
                        {links.map(({ text, to }, key) => (
                            <div key={key} className="header-section-link">
                                <Link to={to}>{text}</Link>
                            </div>
                        ))}
                    </div>
                    {/* <div className="header-sign-up-button">
                        <RadianButton href={config.settings.header.signUp} variant="outline" color="dark-blue">
                            Sign Up
                        </RadianButton>
                    </div> */}
                </div>
                <SvgIconButton
                    className="header-menu-button"
                    icon={isOpen ? 'HeaderMenuCloseIcon' : 'HeaderMenuIcon'}
                    onClick={() => setIsOpen(value => !value)}
                />
            </div>
            <div className={clsx('header-menu', { isOpen })}>
                <div className="overlay" onClick={() => setIsOpen(false)} />
                <div className="header-menu-column-list">
                    {links.map(({ text, to }, key) => (
                        <Link to={to} key={key}>
                            {text}
                        </Link>
                    ))}
                    {/* <RadianButton href={config.settings.header.signUp} variant="outline" color="dark-blue" style={{width: '150px', padding:'15px 20px'}}>
                        Sign Up
                    </RadianButton> */}
                </div>
            </div>
        </HeaderWrapper>
    );
};

Header.propTypes = {
    Logo: PropTypes.element,
    links: PropTypes.array,
    logoSrc: PropTypes.string,
    nav: PropTypes.object,
};

export default Header;
