import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import svgs from '../../config/svgsBinding';
import RadianButton from '../Button/RadianButton';
import { Drawer, DrawerBody, DrawerOverlay, DrawerContent, useDisclosure } from '@chakra-ui/core';

const headerLogoSrc = svgs['HeaderLogoImage'];

const Wrapper = styled.div`
    display: none;
    position: relative;
    z-index:2;
    #logo {
        width: 150px;
        img {
            object-fit: cover;
            height: 100%;
            width: 100%;
            margin: 0;
        }
    }
    @media (max-width: 1440px) {
    }
    @media (max-width: 1024px) {
    }
    @media (max-width: 769px) {
        display: flex;
    }
    @media (max-width: 481px) {
    }
    @media (max-width: 376px) {
    }
`;

const MenuButtonWrapper = styled.span`
    padding: 1rem;
    font-size: 1.5rem;
    cursor: pointer;
    color: var(--text-link);
`;

const DrawerContentWrapper = styled(DrawerContent)`
    background: #fff !important;
    .menu-list {
        padding: 0 !important;
    }
    .container {
        z-index: 1;
        padding-top: 20px;
        padding-bottom: 20px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            padding: 20px 14px;
        }
        @media (max-width: 376px) {
        }
    }
    .column-list {
        display: flex;
        flex-direction: column;
        padding: 20px;
        background-color: #fff;
        z-index: 10000;
        a,
        button {
            margin: 23px 20px;
            font-family: Inter;
            font-style: normal;
            font-weight: 600;
            font-size: 14px;
            line-height: 110%;
            /* or 15px */

            letter-spacing: 0.02em;
            text-transform: uppercase;
        }
    }
`;

const HeaderDrawer = ({ links = [] }) => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    return (
        <Wrapper>
            {!isOpen && (
                <MenuButtonWrapper onClick={onOpen} className="menu-btn">
                    <img height={40} width={40} src={svgs['HeaderMenuIcon']} alt="menu" />
                </MenuButtonWrapper>
            )}
            <Drawer placement={'top'} onClose={onClose} isOpen={isOpen}>
                <DrawerOverlay />
                <DrawerContentWrapper>
                    <DrawerBody className="menu-list">
                        <div className="container flex between">
                            <div id="logo">
                                <Link to="/">
                                    <img
                                        alt="logo"
                                        src={headerLogoSrc}
                                        width="100%"
                                        height="100%    "
                                    />
                                </Link>
                            </div>
                            <MenuButtonWrapper onClick={onClose} className="menu-btn">
                                <img
                                    height={40}
                                    width={40}
                                    src={svgs['HeaderMenuCloseIcon']}
                                    alt="menu"
                                />
                            </MenuButtonWrapper>
                        </div>

                        <div className="column-list">
                            {links.map(({ text, to }, key) => (
                                <Link to={to} key={key}>
                                    {text}
                                </Link>
                            ))}
                            <RadianButton variant="outline" color="dark-blue" style={{width: '150px', padding:'15px 20px'}}>
                                Sign Up
                            </RadianButton>
                        </div>
                    </DrawerBody>
                </DrawerContentWrapper>
            </Drawer>
        </Wrapper>
    );
};

HeaderDrawer.propTypes = {
    links: PropTypes.array,
};

export default HeaderDrawer;
