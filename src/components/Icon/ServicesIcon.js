import React from 'react';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';

import config from '../../config';

const bounceKeyframes = keyframes`
    0%   { transform: scale(1,1)      translateY(0); }
    10%  { transform: scale(1.1,.9)   translateY(0); }
    30%  { transform: scale(.9,1.1)   translateY(-10px); }
    50%  { transform: scale(1.05,.95) translateY(0); }
    57%  { transform: scale(1,1)      translateY(-2px); }
    64%  { transform: scale(1,1)      translateY(0); }
    100% { transform: scale(1,1)      translateY(0); }
`;

const Wrapper = styled.div`
    /* padding:5px; */
    display: flex;
    justify-content: center;
    align-items: center;
    margin:-5px;
    /* &:hover {
        animation-name: ${bounceKeyframes};
        animation-duration: 2s;
        animation-timing-function: cubic-bezier(0.28, 0.84, 0.42, 1);
    } */
    img,
    svg {
        position: relative;
        max-width: 60px;
        max-height: 60px;
        height: 100%;
        width: 100%;
        @media (max-width: 1024px) {
            /* transform:scale(0.7); */
            /* max-width: 46px;
            max-height: 46px; */
        }
        /* margin:auto; */
        /* border-radius: 50%; */
        /* background: #fff; */
        /* padding: 5px; */
        /* box-shadow: 0px 0px 10px rgba(28, 28, 36, 0.1), 0px 4px 4px rgba(28, 28, 36, 0.1); */
    }
`;

const ServiceIcon = ({ icon }) => {
    return (
        <Wrapper>
            <img src={config.svgs[icon]} width="46" height="46" alt="service" />
        </Wrapper>
    );
};

ServiceIcon.propTypes = {
    icon: PropTypes.string,
};

export default ServiceIcon;
