import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

const NavLink = ({ to, href, children, ...rest }) => {
    if (href) {
        return (
            <a {...rest} href={href}>
                {children}
            </a>
        );
    }

    return (
        <Link to={to} {...rest}>
            {children}
        </Link>
    );
};

NavLink.propTypes = {
    to: PropTypes.string,
    href: PropTypes.string,
    children: PropTypes.node,
};

export default NavLink;
