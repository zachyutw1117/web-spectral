import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import config from '../../config';
// import BG from '../../assets/LoaderImage.svg';

const PageLoaderContent = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    padding: 10% 20%;
    height: 100%;
    min-height: 100%;
    min-width: 100%;
    width: 100%;
    opacity: 0.8;
    background: rgba(255, 255, 255, 0.8);
`;

const PageLoader = ({ isLoading }) => {
    return (
        <PageLoaderContent style={{ display: !isLoading && 'none' }}>
            <img src={config.svgs['LoaderImage']} alt="loader" />
            {/* <BG width="100%" height="100%" viewBox="0 0 100% 100%" /> */}
        </PageLoaderContent>
    );
};

PageLoader.propTypes = {
    isLoading: PropTypes.bool,
};

export default PageLoader;
