export const homeSectionPrefix = name => `#home-section-${name}`;

export const getNavs = (nav, prefix) =>
    Object.values(nav).map(({ name }) => ({ text: name, to: prefix(name) }));

export const svgToUrl = svgTxt => {
    return `data:image/svg+xml;base64,${btoa(svgTxt)}`;
};
