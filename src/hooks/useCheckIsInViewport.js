import { useEffect, useCallback, useState } from 'react';

const useCheckIsInViewport = (ref, preLoadPx = 500) => {
    const [isViewport, setIsViewport] = useState(false);

    const activeCheck = useCallback(() => {
        if (!ref) {
            setIsViewport(false);
        } else {
            const bounding = ref.current.getBoundingClientRect();
            setIsViewport(
                bounding.top + preLoadPx >= 0 &&
                    bounding.bottom - preLoadPx <=
                        (window.innerHeight || document.documentElement.clientHeight)
            );
        }
    }, [ref, preLoadPx]);

    useEffect(() => {
        const bounding = ref.current.getBoundingClientRect();
        setIsViewport(
            bounding.top >= 0 &&
                bounding.left >= 0 &&
                bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)
        );
        window.addEventListener('scroll', activeCheck, {
            capture: true,
            passive: true,
        });
        return () => {
            window.removeEventListener('scroll', activeCheck, {
                capture: true,
                passive: true,
            });
        };
    }, []);
    return isViewport;
};

export default useCheckIsInViewport;
