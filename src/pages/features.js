import React from 'react';
import { ThemeProvider, theme } from '@chakra-ui/core';
import styled from 'styled-components';

import Layout from '../components/layout';
import SEO from '../components/seo';
import RadianButton from '../components/Button/RadianButton';
import config from '../config';
import FeatureStackCard from '../components/Card/FeatureStackCard';
import CubesBannerCard from '../components/Card/CubesBannerCard';
import ChakraHubspotFormDialog from '../components/Dialog/ChakraHubspotFormDialog';

const { s01, s02, s03, s04 } = config.settings.features.section;

const Section01 = styled.section`
    &#${s01.name} {
        position: relative;
        padding-top: 65px;
        padding-bottom: 150px;
        color: var(--text-link);
        overflow: hidden;
        @media (max-width: 1024px) {
            padding-bottom: 0;
        }
        @media (max-width: 769px) {
            padding-top: 0;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .s01-header-container {
            position: relative;
            display: flex;
            z-index: 1;

            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                flex-direction: column-reverse;
                padding-bottom: 0;
            }
            @media (max-width: 769px) {
                padding-bottom: 0;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }

            & > .bg {
                position: relative;
                background-position: right;
                display: flex;
                justify-content: flex-end;
                width: 100%;
                padding-top: 25px;
                transform: translateX(35px);
                @media (max-width: 1204px) {
                    justify-content: center;
                    transform: unset;
                }
                .wrapper {
                    position: relative;
                }
                & > .wrapper img.layer1,
                & > .wrapper svg.layer1,
                & > .wrapper .lottie.layer1 {
                    object-fit: cover;
                    max-width: 690px;
                    width: 100% !important;
                    height: auto !important;
                    z-index: 1;
                    @media (max-width: 1024px) {
                        max-width: 612px;
                        width: 100% !important;
                        height: auto !important;
                    }
                }
                & > .wrapper img.filter,
                & > .wrapper svg.filter,
                & > .wrapper .lottie.filter {
                    position: absolute;
                    top: 10.45%;
                    right: 7%;
                    width: 72%;
                    z-index: 3;
                }
                & > .wrapper img.solis,
                & > .wrapper svg.solis,
                & > .wrapper .lottie.solis {
                    position: absolute;
                    top: 20%;
                    right: 18%;
                    width: 60%;
                    z-index: 3;
                }
                & > .wrapper .layer2 {
                    position: absolute;
                    top: 10.75%;
                    right: 6%;
                    z-index: 2;
                    min-width: 74.4%;
                    height: 81%;
                    background: #ecf3f9;
                }
            }
            .s01-content {
                flex-basis: 40%;
                z-index: 1;
                padding: 0;

                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    flex-basis: 100%;
                    text-align: center;
                    padding: 32px;
                }
                @media (max-width: 769px) {
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
                h1 {
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                        width: 100%;
                    }
                    @media (max-width: 769px) {
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                    }
                }
                p {
                    margin: 0px;
                    width: 555px;
                    @media (max-width: 1024px) {
                        text-align: center;
                    }
                    @media (max-width: 1024px) {
                        width: 100%;
                    }
                }
                .button-group {
                    display: flex;
                    margin-top: 54px;
                    @media (max-width: 1440px) {
                        margin-top: 76px;
                    }
                    @media (max-width: 1024px) {
                        justify-content: center;
                        margin-top: 86px;
                    }
                    @media (max-width: 769px) {
                        margin-top: 54px;
                    }
                    @media (max-width: 630px) {
                        flex-direction: column;
                        width: 100%;
                    }
                    @media (max-width: 481px) {
                        padding-left: 45px;
                        padding-right: 45px;
                    }
                    @media (max-width: 376px) {
                        padding-left: 14px;
                        padding-right: 14px;
                    }
                    & > .button:nth-child(even) {
                        margin-left: 24px;
                        @media (max-width: 1440px) {
                        }
                        @media (max-width: 1024px) {
                            margin-left: 0;
                            margin-top: 24px;
                        }
                        @media (max-width: 769px) {
                        }
                        @media (max-width: 630px) {
                        }
                        @media (max-width: 481px) {
                        }
                        @media (max-width: 376px) {
                        }
                    }
                    .button {
                        font-size: 20px;
                        /* padding: 14px 54px; */
                        text-align: center;
                        @media (max-width: 1440px) {
                        }
                        @media (max-width: 1024px) {
                        }
                        @media (max-width: 769px) {
                        }
                        @media (max-width: 481px) {
                            flex: 1 1;
                            margin-left: 0;
                            width: 100%;
                            max-width: 300px;
                        }
                        @media (max-width: 376px) {
                            padding: 14px 40px;
                        }
                    }
                }
            }
        }
    }
`;

const Section02 = styled.section`
    &#${s02.name} {
        position: relative;
        z-index: 1;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            padding-bottom: 200px;
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .s02-container {
            position: relative;
            padding-top: 105px;
            padding-left: 81px;
            padding-right: 81px;
            padding-bottom: 250px;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            z-index: 1;

            @media (max-width: 1440px) {
                padding-top: 32px;
            }
            @media (max-width: 1024px) {
                padding-left: 32px;
                padding-right: 32px;
                padding-top: 150px;
                padding-bottom: 100px;
            }
            @media (max-width: 769px) {
                padding-top: 32px;
                padding-bottom: 32px;
                flex-direction: column-reverse;
                align-items: center;
                justify-content: center;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
            button {
                padding: 16px 32px;
                @media (max-width: 760px) {
                    width: 100%;
                    padding: 14px 32px;
                }
            }

            .s02-card-bg {
                flex: 1 1;
                z-index: 0;
                /* height: 402px; */
                display: flex;
                justify-content: flex-end;
                width: auto;
                padding-top: 0;
                @media (max-width: 769px) {
                    padding-top: 1rem;
                }
            }
            .s02-content {
                display: flex;
                justify-content: center;
                align-items: flex-start;
                flex-direction: column;
                flex-basis: 50%;
                z-index: 1;
                max-width: 560px;
                padding: 0;
                @media (max-width: 1024px) {
                    text-align: center;
                    flex-basis: 100%;
                    align-items: center;
                }
                p {
                    font-size: 20px;
                    width: 560px;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        width: 90%;
                        font-size: 18px;
                        margin: 8px 0;
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                        font-size: 16px;
                    }
                }
            }
            &.reverse {
                flex-direction: row-reverse;
                @media (max-width: 769px) {
                    flex-direction: column-reverse;
                }
                .s02-card-bg {
                    z-index: 0;
                    justify-content: flex-start;
                    @media (max-width: 769px) {
                        position: relative;
                        padding-top: 1rem;
                        display: flex;
                        justify-content: center;
                        height: auto;
                        margin-right: 0;
                        transform: translateX(0);
                    }
                }
            }
            &.knowYourBlindspots .s02-card-bg {
                /* height: 402px; */
                padding-top: 30px;
                @media (max-width: 769px) {
                    padding-top: 48px;
                    padding: 0;
                }
                & > img {
                    max-height: 538.2px;
                    max-width: 373px;
                    height: auto;
                    width: 100%;
                    @media (max-width: 1024px) {
                        max-height: unset;
                        max-width: 373px;
                    }
                    @media (max-width: 769px) {
                    }
                }

                & > svg,
                & .lottie {
                    width: 100%;
                    max-height: 538.2px;
                    max-width: 373px;
                    height: auto;
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        max-width: 90vw;
                    }
                }
                & .lottie {
                    width: 100%;
                }
            }
            &.humanErrors .s02-card-bg {
                /* height: 402px; */
                /* padding-right: 65px;  */

                @media (max-width: 769px) {
                    margin-top: 48px;
                    padding: 0;
                }
                & > img,
                & > svg,
                & .lottie {
                    /* max-height: 481.59px; */
                    max-width: 518.51px;
                    width: 100%;
                    height: auto;
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        max-width: 90vw;
                    }
                }
            }

            &.brightIdea .s02-card-bg {
                padding-left: 50px;
                @media (max-width: 1024px) {
                    padding-left: 0;
                }
                @media (max-width: 769px) {
                    margin-top: 48px;
                    padding: 0;
                }
                @media (max-width: 769px) {
                }
                /* height: 402px; */
                & > img,
                & > svg,
                & .lottie {
                    max-height: 538.2px;
                    max-width: 343px !important;
                    width: 100%;
                    height: auto;
                }

                & > img {
                    transform: translateX(40px) translateY(40px);
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        transform: unset;
                        max-width: 70vw !important;
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                    }
                }
                & .lottie {
                    transform: scale(1.3) translateY(-10px);
                }
            }
        }
    }
`;

const SectionS04 = styled.section`
    &#${s04.name} {
    }
`;
const Section03 = styled.section`
    &#${s03.name} {
        position: relative;
        width: 100%;
        z-index: 1;
        padding: 210px 0 210px 0;
        overflow: hidden;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            padding: 210px 0 230px 0;
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .container {
            position: relative;
            .content {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                & > h2 {
                    text-align: center;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        max-width: 500px;
                    }
                    @media (max-width: 481px) {
                        max-width: 400px;
                    }
                    @media (max-width: 376px) {
                        max-width: 320px;
                    }
                }

                & > p {
                    text-align: center;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        max-width: 500px;
                    }
                    @media (max-width: 481px) {
                        max-width: 400px;
                    }
                    @media (max-width: 376px) {
                        max-width: 320px;
                    }
                }
            }
            .stack-cards {
                margin-top: 150px;
                width: 100%;
                padding: 0 23px;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-evenly;
                @media (max-width: 769px) {
                    margin-top: 48px;
                }
                & > * {
                    margin: 31px;
                    @media (max-width: 1440px) {
                        margin: 10px 0;
                    }
                    @media (max-width: 1024px) {
                        margin: 20px 5px 20px 5px;
                    }
                    @media (max-width: 769px) {
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                    }
                }
            }
            .bg {
                position: absolute;
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                img.leftCubes {
                    position: absolute;
                    bottom: 0;
                    left: 0;
                }
                img.rightCubes {
                    position: absolute;
                    bottom: 0;
                    right: 0;
                }
            }
        }
    }
`;

const PageContent = styled.div`
    color: var(--text-link);
    overflow-x: hidden;
    background: #fff;
    & > section {
        animation: section-fade 500ms ease-in-out;
    }
    background-image: url('${config.svgs['FeatureS02CurveBgImage']}');
    background-repeat: no-repeat;
    background-size: auto 3100px;
    background-position: 0 300px;
    @media (min-width: 1930px) {
        background-size: auto 3600px;
    }
    @media (min-width: 2200px) {
        background-size: cover;
    }
   
`;

const SecondPage = () => {
    return (
        <Layout>
            <ThemeProvider theme={theme}>
                <SEO title="Our Features | Spectra" />
                <PageContent>
                    {/* <div className="curve-background-image">
                        <img src={config.svgs['FeatureS02CurveBgImage']} alt="curve-background" />
                    </div> */}
                    <Section01 id={s01.name}>
                        <div className="container s01-header-container">
                            <div className="s01-content">
                                <h1>{s01.h1}</h1>
                                <p className="sub-main">{s01.description}</p>
                                <div className="button-group">
                                    <ChakraHubspotFormDialog
                                        trigger={dialogProp => (
                                            <RadianButton {...dialogProp}>
                                                {s01.buttonGroup.getStart.text}
                                            </RadianButton>
                                        )}
                                    />
                                    {/* <RadianButton
                                        {...s01.buttonGroup.learnMore}
                                        variant="outline"
                                    /> */}
                                </div>
                            </div>
                            <div className="bg">
                                <div className="wrapper">
                                    <img
                                        className="layer1"
                                        src={config.images['FeatureS01Image']}
                                        alt="s01"
                                    />
                                    {/* <div className="layer2" /> */}
                                    {/* <img
                                        className="filter"
                                        src={config.svgs['FeatureS01FilterImage']}
                                        alt="filter"
                                    />
                                    <img
                                        className="solis"
                                        src={config.svgs['FeatureS01SolisImage']}
                                        alt="solis"
                                    /> */}
                                </div>
                            </div>
                        </div>
                    </Section01>
                    <Section02 id={s02.name}>
                        <div className="s02-container container reverse knowYourBlindspots">
                            <div className="s02-content">
                                <h2>{s02.knowYourBlindspots.h2}</h2>
                                <p>{s02.knowYourBlindspots.p}</p>
                            </div>
                            <div className="s02-card-bg">
                                <img
                                    src={config.svgs['FeatureS02KnowYourBlindspotsImage']}
                                    alt="know your blindspots"
                                />
                                {/* {config.renderLotties['FeatureS02KnowYourBlindspotsImage']} */}
                            </div>
                        </div>
                        <div className="s02-container container humanErrors">
                            <div className="s02-content">
                                <h2>{s02.humanErrors.h2}</h2>
                                <p>{s02.humanErrors.p}</p>
                            </div>
                            <div className="s02-card-bg">
                                <img
                                    src={config.images['FeatureS02HumanErrorsImage']}
                                    alt="human errors"
                                />
                                {/* {config.renderLotties['FeatureS02HumanErrorsImage']} */}
                            </div>
                        </div>
                        <div className="s02-container container reverse brightIdea">
                            <div className="s02-content">
                                <h2>{s02.brightIdea.h2}</h2>
                                <p>{s02.brightIdea.p}</p>
                            </div>
                            <div className="s02-card-bg">
                                <img
                                    src={config.images['FeatureS02BrightIdeaImage']}
                                    alt="bright idea"
                                />
                                {/* {config.renderLotties['FeatureS02BrightIdeaImage']} */}
                            </div>
                        </div>
                    </Section02>
                    <SectionS04 id={s04.name}>
                        <div className="container">
                            <CubesBannerCard data={s04} />
                        </div>
                    </SectionS04>
                    <Section03 id={s03.name}>
                        <div className="container">
                            <div className="content">
                                <h2>{s03.h2}</h2>
                                <p className="sub-main">{s03.p}</p>
                                <div className="stack-cards">
                                    {s03.stacks.map((data, index) => (
                                        <FeatureStackCard key={index} data={data} index={index} />
                                    ))}
                                </div>
                            </div>
                        </div>
                    </Section03>
                </PageContent>
            </ThemeProvider>
        </Layout>
    );
};

export default SecondPage;
