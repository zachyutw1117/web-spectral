import React from 'react';
import { ThemeProvider, theme } from '@chakra-ui/core';
import styled from 'styled-components';
import Layout from '../components/layout';
import SEO from '../components/seo';
// import CharacteristicCard from '../components/Card/CharacteristicCard';
// import EmployeeCard from '../components/Card/EmployeeCard';
// import CubesBannerCard from '../components/Card/CubesBannerCard';
import FindUsCard from '../components/Card/FindUsCard';
import config from '../config';
import svgs from '../config/svgsBinding';
const {
    s01,
    // s02,
    s03,
    //  s05,
    s06,
} = config.settings.about.section;

const Section01 = styled.section`
    &#${s01.name} {
        position: relative;

        padding-top: 20px;
        padding-bottom: 20px;
        color: var(--text-link);
        .container {
            align-items: center;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                flex-direction: column-reverse;
                justify-content: flex-start;
                align-items: center;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
            }
            h1 {
                width: 765px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    width: 412px;
                    z-index: 1;
                }
                @media (max-width: 769px) {
                    width: 100%;
                    padding: 0 14px;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
            }
            img {
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    width: 90%;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
            }
        }
    }
`;

// const Section02 = styled.section`
//     &#${s02.name} {
//         position: relative;
//         z-index: 1;
//         padding-top: 30px;
//         padding-bottom: 30px;
//         .characteristic-card-group {
//             @media (max-width: 1440px) {
//             }
//             @media (max-width: 1024px) {
//             }
//             @media (max-width: 769px) {
//             }
//             @media (max-width: 481px) {
//                 flex-wrap: wrap;
//                 & > div {
//                     width: 50%;
//                 }
//             }
//             @media (max-width: 376px) {
//             }
//         }
//     }
// `;

const Section03 = styled.section`
    &#${s03.name} {
        position: relative;
        z-index: 1;
        padding-top: 210px;
        padding-bottom: 210px;
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            padding-top: 100px;
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
            padding-top: 110px;
        }
        @media (max-width: 376px) {
        }

        .section-content {
            padding-left: 150px;
            padding-right: 150px;
            align-items: flex-start;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
            }
            @media (max-width: 769px) {
                padding-left: 14px;
                padding-right: 14px;
            }
            @media (max-width: 481px) {
                flex-direction: column;
                padding-left: 14px;
                padding-right: 14px;
            }
            @media (max-width: 376px) {
            }
            h2 {
                margin-top: 0px;
                margin-right: 30px;
                width: 199px;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    margin-top: 8px;
                }
                @media (max-width: 481px) {
                    width: 90%;
                }
                @media (max-width: 376px) {
                }
            }
            .description {
                flex: 1 1;
                p {
                    font-family: Inter;
                    font-style: normal;
                    font-weight: normal;
                    font-size: 20px;
                    line-height: 38px;
                    color: #122d5f;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                    }
                    @media (max-width: 769px) {
                        font-size: 18px;
                        line-height: 190%;
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                        font-size: 16px;
                    }
                }
            }
            p {
                /* white-space: pre-wrap; */
                padding-bottom: 30px;
            }
        }
    }
`;

// const Section04 = styled.section`
//     &#${s04.name} {
//         position: relative;
//         z-index: 1;
//         padding-bottom: 180px;
//         .employee-card-group {
//             flex-wrap: wrap;
//         }
//     }
// `;

// const Section05 = styled.section`
//     &#${s05.name} {
//         position: relative;
//         z-index: 1;
//         padding-bottom: 180px;
//         @media (max-width: 1440px) {
//         }
//         @media (max-width: 1024px) {
//         }
//         @media (max-width: 769px) {
//         }
//         @media (max-width: 481px) {
//             padding-bottom: 0;
//         }
//         @media (max-width: 376px) {
//         }
//     }
// `;

const Section06 = styled.section`
    &#${s06.name} {
        position: relative;
        z-index: 1;
        overflow: hidden;
        background: linear-gradient(
            184.1deg,
            #091e43 -7.85%,
            #021f56 16.35%,
            #001a4b 50.91%,
            #022c7a 88.38%
        );
        .bg {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 0;
            img {
            }
        }
        .container {
            z-index: 1;
            height: 787px;
            display: flex;
            justify-content: center;
            align-items: center;
            padding-left: 30px;
            padding-right: 30px;
            .section-content {
                width: 969px;
                z-index: 1;
            }
        }
    }
`;

const PageContent = styled.div`
    position: relative;
    overflow: hidden;
    color: var(--text-link);
    background: #fff;
    & > section {
        animation: section-fade 500ms ease-in-out;
    }
    background-image: url('${config.svgs['AboutBackgroundImage']}');
    background-repeat: no-repeat;
    background-size:auto 2560px;
    background-position: 0 500px;
    @media (min-width:2000px){
        background-size:cover;
    }
    
    @media (max-width: 1440px) {
    }
    @media (max-width: 1024px) {
        background-position: 0 400px;
    }
    @media (max-width: 769px) {
        
        background-position: -300px 500px;
    }
    @media (max-width: 481px) {
        background-position: -600px 180px;
    }
    @media (max-width: 376px) {
    }
   
`;

const SecondPage = () => {
    return (
        <Layout>
            <ThemeProvider theme={theme}>
                <SEO title="About Us | Spectral" />
                <PageContent>
                    <Section01 id={s01.name}>
                        <div className="container flex">
                            <h1>{s01.title}</h1>
                            <img src={svgs['AboutS01HeaderImage']} alt="headerImage" />
                        </div>
                    </Section01>
                    {/* <Section02 id={s02.name}>
                        <div className="container">
                            <div className="characteristic-card-group flex evenly">
                                {s02.characteristic.map((value, index) => (
                                    <CharacteristicCard key={index} data={value} index={index} />
                                ))}
                            </div>
                        </div>
                    </Section02> */}
                    <Section03 id={s03.name}>
                        <div className="container">
                            <div className="section-content flex">
                                <h2>{s03.title}</h2>
                                <div
                                    className="description"
                                    dangerouslySetInnerHTML={{ __html: s03.description }}
                                />
                            </div>
                        </div>
                    </Section03>
                    {/* <Section04 id={s04.name}>
                        <div className="container">
                            <div className="employee-card-group flex center">
                                {s04.employees.map((value, index) => (
                                    <EmployeeCard key={index} data={value} />
                                ))}
                            </div>
                        </div>
                    </Section04> */}
                    {/* <Section05 id={s05.name}>
                        <div className="container">
                            <CubesBannerCard data={s05} />
                        </div>
                    </Section05> */}
                    <Section06 id={s06.name}>
                        <div className="bg">
                            <img src={svgs['AboutS06BgImage']} alt="background" />
                        </div>
                        <div className="container">
                            <div className="section-content">
                                <FindUsCard data={s06} />
                            </div>
                        </div>
                    </Section06>
                </PageContent>
            </ThemeProvider>
        </Layout>
    );
};

export default SecondPage;
