import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider, theme } from '@chakra-ui/core';
import styled from 'styled-components';

import Layout from '../components/layout';
import SEO from '../components/seo';

import privacyPolicy from '../config/content/privacyPolicy.json';
import termOfUse from '../config/content/termOfUse.json';
import securityRiskDetectionToolLicense from '../config/content/securityRiskDetectionToolLicense.json';

const PageContent = styled.div`
    color: var(--text-link);
    overflow-x: hidden;
    background: #fff;

    .legal-content {
        display: flex;
        justify-content: center;
        width: 100%;
        flex-wrap: wrap;
        padding: 14px;
        & > section {
            flex: 1 1 100%;
            display: flex;
            justify-content: flex-start;
            flex-direction: column;
            align-items: flex-start;
            max-width: 900px;
            width: 100%;
            margin-bottom: 1rem;
        }
        &.privacy-policy {
            justify-content: flex-start;
            flex-direction: column;
            align-items: center;
        }
        .date {
            align-items: flex-start;
        }
        h1 {
            font-size: 1.1rem;
            font-weight: bold;
            color: #000000;
            text-decoration: underline;
        }

        h2 {
            font-size: 1.2rem;
            font-weight: bold;
            color: #1f497d;
        }
        h3 {
            font-size: 1rem;
            color: #000000;
            text-decoration: underline;
            font-weight: 800;
            margin: 16px 0;
        }
        h4 {
            font-size: 1rem;
            color: black;
            font-weight: 400;
            margin: 16px 0;
        }
        h5 {
            font-size: 1rem;
            color: black;
            font-weight: 800;
            margin: 16px 0;
        }
        p {
            font-size: 1rem;
            font-style: normal;
            font-weight: normal;
            color: #000000;
            margin-bottom: 7px;
        }
        a {
            padding: 0 0.2rem;
            color: blue;
            text-decoration: underline;
        }
        sub {
            font-size: 1rem;
        }
        li {
            color: #000000;
            margin-bottom: 0.5rem;
        }
        section > ol {
            padding-left: 30px;
            & > li {
                font-weight: 800;
                & > ol {
                    font-weight: normal;
                    margin: 1rem 0 1rem 2rem;
                    & > {
                        margin: 1rem 0;
                    }
                }
            }
        }
        ol {
            counter-reset: item;
        }
        ol li {
            display: block;
            position: relative;
        }
        ol li:before {
            content: counters(item, '.') '.';
            counter-increment: item;
            position: absolute;
            margin-right: 100%;
            right: 10px; /* space between number and text */
        }
        .grid {
            display: grid;
            grid-template-columns: 300px auto;
            /* grid-template-rows: 80px auto 80px; */
            column-gap: 1rem;
            row-gap: 2rem;
            /* margin: 0.5rem 0; */
            @media (max-width: 769px) {
                grid-template-columns: 200px auto;
            }
            @media (max-width: 481px) {
                grid-template-columns: 150px auto;
            }
            @media (max-width: 376px) {
                grid-template-columns: 120px auto;
            }
        }
    }
`;

const docs = {
    'privacy policy': privacyPolicy,
    'term of use': termOfUse,
    'security risk detection tool license': securityRiskDetectionToolLicense,
};
const LegalPage = ({ location }) => {
    const ref = React.useRef(null);
    const doc = docs[decodeURIComponent(location.search.replace('?doc=', ''))] || {};
    // React.useEffect(() => {
    //     console.log(JSON.stringify({ html: ref.current.innerHTML }));
    // }, []);

    return (
        <Layout>
            <ThemeProvider theme={theme}>
                <SEO title="Our Features | Spectra" />
                <PageContent>
                    <div
                        ref={ref}
                        className="container"
                        dangerouslySetInnerHTML={{
                            __html: doc.html || `<div class="legal-content" />`,
                        }}
                    ></div>
                </PageContent>
            </ThemeProvider>
        </Layout>
    );
};

LegalPage.propTypes = {
    location: PropTypes.any,
};

export default LegalPage;
