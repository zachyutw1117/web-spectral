import React from 'react';
import { ThemeProvider, theme } from '@chakra-ui/core';
import styled from 'styled-components';
import Layout from '../components/layout';
import SEO from '../components/seo';
import PromotionCard from '../components/Card/PromotionCard';
// import CodeDemo from '../components/Code/CodeDemo';
import RadianButton from '../components/Button/RadianButton';
import FoxChartBannerCard from '../components/Card/FoxChartBannerCard';
import HeaderAnimationCard from '../components/Card/HeaderAnimationCard';
import IntroductionAnimationCard from '../components/Card/IntroductionAnimationCard';
import CardContent from '../components/Card/CardContent';
import StackServiceCard from '../components/Card/StackServiceCard';
import NewsGroupCard from '../components/Card/NewsGroupCard';
import ChakraHubspotFormDialog from '../components/Dialog/ChakraHubspotFormDialog';

import config from '../config';
import '../styles/index.scss';
const { s01, s02, s03, s04, s05, foxChart } = config.settings.home.section;
//

const Section01 = styled.section`
    &#${s01.name} {
        position: relative;
        padding-top: 20px;
        color: var(--text-link);
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            padding-top: 0;
            margin-top: -10px;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .s01-header-container {
            position: relative;
            padding-bottom: 300px;
            display: flex;
            @media (max-width: 1204px) {
                flex-direction: column;
                padding-bottom: 80px;
            }
            @media (max-width: 769px) {
                padding-bottom: 80px;
            }
            & > .bg {
                position: relative;
                height: auto;
                background-position: right;
                display: flex;
                width: 100%;
                transform: scale(2) translateY(50px) translateX(50px);
                @media (max-width: 1404px) {
                    transform: scale(2.5) translateY(50px) translateX(0);
                }
                @media (max-width: 1204px) {
                    transform: scale(1.2);
                }
                & > img,
                & > svg,
                & > .lottie {
                    max-width: 1200px;
                    width: 100%;
                    height: auto;
                    @media (max-width: 1140px) {
                        max-width: 1000px;
                    }
                }
            }
            .s01-content {
                flex-basis: 40%;
                padding: 2em 0;
                @media (max-width: 1024px) {
                    flex-basis: 100%;
                    text-align: center;
                }
                h1 {
                    z-index: 2;
                    width: 765px;
                    /* width:100%; */
                    @media (max-width: 769px) {
                        width: 100%;
                    }
                }

                .s01-button-group {
                    margin: 50px 0;
                    display: flex;
                    & > *:nth-child(even) {
                        margin-left: 24px;
                    }
                    @media (max-width: 1024px) {
                        justify-content: center;
                    }
                    @media (max-width: 769px) {
                        margin: 2rem 0;
                    }
                    @media (max-width: 430px) {
                        flex-direction: column;
                        align-items: center;
                        & > *:nth-child(even) {
                            margin-left: 0;
                            margin-top: 1rem;
                        }
                    }
                    .button {
                        max-width: 233px;
                        width: 40%;

                        font-size: 20px;
                        @media (max-width: 769px) {
                            font-size: 14px;
                            padding-top: 14px;
                            padding-bottom: 14px;
                        }
                        @media (max-width: 430px) {
                            flex-direction: column;
                            align-items: center;
                            max-width: 233px;
                        }
                        @media (max-width: 376px) {
                        }
                    }
                }
            }
        }
        .s01-divide-container {
            padding-top: 50px;
            margin-bottom: 75px;
            @media (max-width: 1440px) {
                padding-top: 20px;
            }
            @media (max-width: 1024px) {
                margin-bottom: 1rem;
                text-align: center;
                width: 100%;
            }
            @media (max-width: 769px) {
                margin-top: 20px;
                max-width: 520px;
            }
            @media (max-width: 481px) {
                max-width: 400px;
            }
            @media (max-width: 376px) {
                margin-bottom: 14px;
                max-width: 320px;
            }
        }
        .s01-promotion-container {
            padding-bottom: 450px;
            @media (max-width: 1440px) {
                padding-bottom: 470px;
            }
            @media (max-width: 1024px) {
                padding-top: 100px;
                padding-bottom: 381px;
            }
            @media (max-width: 769px) {
                padding-bottom: 870px;
            }
            @media (max-width: 481px) {
            }
            @media (max-width: 376px) {
                padding-bottom: 800px;
            }
            .s01-promotion {
                display: flex;
                justify-content: space-around;
                align-items: flex-start;
                width: 100%;
                @media (max-width: 769px) {
                    flex-direction: column;
                    align-items: center;
                }
            }
        }
    }
`;

const Section02 = styled.section`
    &#${s02.name} {
        position: relative;
        padding-top: 300px;
        /* background: #fff; */
        z-index: 0;
        padding-bottom: 145px;
        background-image:url('${config.svgs['S02BgImage']}');
        background-size:auto 3609.42px;
        background-repeat:no-repeat;
        background-origin:center;

        @media (min-width:1500px){
            background-size:cover;
        }
    
        @media( max-width:1440px){
            background-size:auto 3609.42px;
            
        }
        @media (max-width: 1024px) {
            padding-top: 230px;
            padding-bottom: 55px;
        }
        @media (max-width: 769px) {
            padding-bottom: 32px;
            padding-top: 35px;
        }
        @media (max-width: 481px) {
            padding-top: 65px;
        }
        @media (max-width: 376px) {
            padding-bottom: 0;
            margin-bottom:-10px;
        }
        .developerFirst{
            .card-image img {
                @media (max-width: 769px) {
                    max-width:511px;
                }   
                @media (max-width: 481px) {
                    max-width:400px;
                }
                @media (max-width: 376px) {
                    max-width:333px;
                }
            }         
        }
        .production{
            .card-image img {
            @media (max-width: 769px) {
                max-width:484px;
            }   
            @media (max-width: 481px) {
                max-width:354px;    
            }
            @media (max-width: 376px) {
                max-width:295px;
            }

            }
        }

        .s02-divider-container {
            display: flex;
            flex-direction: column;
            align-items: center;
            z-index: 1;
            text-align: center;
            margin-bottom: 210px;
            margin-top: 190px;
            @media (max-width:1440px){
                margin-top: 170px;
                margin-bottom: 200px;
            }
            @media (max-width: 1024px) {
                margin-top: 160px;  
                margin-bottom: 130px;
            }
            @media (max-width: 769px) {
                margin-bottom: 0;
            }
            @media (max-width: 769px) {
                margin-top: 20px;
                flex-direction: column-reverse;
            }
            @media (max-width: 481px) {
                margin-top: 20px;
                margin-bottom:0;
                max-width:400px;
                width:100%;
            }
            @media (max-width: 376px) {
                margin-top: 0px;
                margin-bottom:-50px;
            }
            h2 {
                color:#11213F;
            }
            .card-content {
                align-items: center;
                margin-bottom: 180px;
                @media (max-width: 1440px) {
                    margin-bottom: 200px;
                }
                @media (max-width: 1024px) {
                    margin-bottom: 180px;
                }
                @media (max-width: 769px) {
                    margin-top: 80px;
                    margin-bottom: 20px;
                }
                @media (max-width: 481px) {
                    margin-top: 60px;
                    margin-bottom: 55px;
                }
                @media (max-width: 376px) {
                }
            }
            p {
                text-align: center;
                color: var(--blue6);
                max-width: 969px;
                width: 100%;
                @media (max-width: 1024px) {
                    font-size:22px;
                }
                @media (max-width: 769px) {
                    font-size: 20px;
                }
                @media (max-width: 481px) {
                    
                }
                @media (max-width: 376px) {
                    font-size: 18px;
                }
            }
            .s02-divider-bg {
                display: flex;
                justify-content: center;
                width: 100%;
                & > img,
                & > svg,
                & .lottie {
                    max-width: 1158px;
                    max-height: 396px;
                    width: 100%;
                    height: auto;
                }
            }
            .s02-divider-content {
                padding: 180px 0 150px 0;
                @media (max-width: 1024px) {
                    padding: 50px 0;
                }
                @media (max-width: 769px) {
                    padding: 1rem 10%;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
            }
        }
    }
`;

const Section03 = styled.section`
    &#${s03.name} {
        position: relative;
        background: var(--dark-back);
        z-index: 2;
        height: 980px;
        padding: 110px 0;

        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
            height: 1000px;
        }
        @media (max-width: 769px) {
            height: 970px;
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .s03-container {
            z-index: 2;
            & h2 {
                /* H2 */
                width: 969px;
                padding: 0 200px;
                /* or 53px */
                text-align: center;
                /* gray */
                color: var(--text-light2);
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    max-width: 520px;
                    padding: 0;
                }
                @media (max-width: 769px) {
                    max-width: 520px;
                }
                @media (max-width: 481px) {
                    max-width: 400px;
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                }
            }
            flex-direction: column;
            color: var(--text-light);
            text-align: center;
            .s03-divide {
                margin: 45px;
                display: flex;
                justify-content: center;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    margin: 45px 0 75px 0;
                }
                @media (max-width: 769px) {
                }
                @media (max-width: 481px) {
                    margin: 25px 0 50px 0;
                }
                @media (max-width: 376px) {
                }
            }
            .s03-image {
                max-width: 868px !important;
                width: 100%;
                background: #ffffff;
                border-radius: 18.3276px;
                padding: 9px;
                box-shadow: 0px 6.87829px 154.762px rgba(17, 113, 167, 0.66);
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    width: 94%;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                }
                img {
                    width: 100%;
                }
            }
        }
    }
`;

const Section04 = styled.section`
    &#${s04.name}{
        position:relative;
        background-color: var(--dark-back);
        background-image: url('${config.svgs['S04Image']}');
        background-repeat: no-repeat;
        background-position: 0 0;
        background-position: center;
        background-size: auto 1067.45px;
        height: 1067.45px;
        display:flex;
        padding-top:185px;
        align-items:center;
        justify-content:center;
        @media (min-width: 3000px) {
            background-color: unset;
            background-size: contain;
        }
        @media (min-width: 2000px) {
            
            background-size: cover;
        }
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            
            /* margin-bottom:43px; */
            /* height: 900px; */
        }
        @media (max-width: 481px) {
            background-size: auto 1649px;
            background-position: center;
            /* height: 1200.45px; */
            height:auto;
            padding-bottom:142px;
        }
        @media (max-width: 376px) {
        }
        .s04-container{
            position:relative;
            flex-direction:column;
            color: var(--dark-back);
            text-align:center;
            h2 {
                @media (max-width: 769px) {
                    font-size: 40px;
                    max-width:520px;
                    width:100%;
                }
                @media (max-width: 481px) {
    
                    max-width: 400px;
                
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                    font-size: 36px;
                }
            }
            p {
                max-width: 863px;
                width: 100%;
                margin-bottom:80px;
                font-family: Inter;
                font-style: normal;
                font-weight: normal;
                font-size: 20px;
                line-height: 176%;
                /* or 35px */

                text-align: center;

                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    max-width:640px;
                    
                    width:100%;
                    
                }
                @media (max-width: 481px) {
                    margin-bottom:50px;
                    max-width: 400px;
                    font-weight: 500;
                
                }
                @media (max-width: 376px) {
                    max-width: 320px;
                    font-size:18px;
                    
                }
            }
            .button-group{
                width:100%;
            }
            .button{
                margin-top:80px;
                max-width: 233px;
                width:100%;
                padding:14px 32px; 
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                }
                @media (max-width: 769px) {
                    /* width:100%;
                    padding:14px 32px; */
                   
                }
                @media (max-width: 481px) {
                    /* margin-top:50px;
                    max-width: 233px; */
                
                }
                @media (max-width: 376px) {
                }
                
            }
            
        }
    }
`;

const Section05 = styled.section`
    &#${s05.name} {
        position: relative;
        width: 100%;
        background: var(--linear-dark-blue);

        
        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
        }
        @media (max-width: 481px) {
        }
        @media (max-width: 376px) {
        }
        .s05-bg {
            position: absolute;
            top: 0;
            width: 0;
            width: 100%;
            height: 100%;
            z-index: 1;
            display: flex;
            overflow: hidden;
            background-image: url('${config.svgs['S05Image']}');
            background-size:auto  725px;
            background-repeat:no-repeat;
            background-position:0 20px;
            @media (min-width:2000px){
                background-size:cover;
                background-position:center;
            }
            img {
                width: 100%;
                z-index: 0;
            }
        }
        .s05-container {
            position: relative;
            flex-direction: column;
            color: var(--text-light);
            z-index: 2;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            overflow: hidden;
            padding-left: 100px;
            padding-right: 27px;
            height: 812px;
            @media (max-width: 1440px) {
            }
            @media (max-width: 1024px) {
                padding-top: 2rem;
                padding-left: 14px;
                padding-right: 14px;
            }
            @media (max-width: 769px) {
                padding-top: 100px;
                padding-left: 0;
                padding-right: 0;
                flex-direction: column;
                height: auto !important;
            }
            @media (max-width: 481px) {
                height: 812px;
            }
            @media (max-width: 376px) {
                padding-top: 56px;
            }
            .s05-content {
                /* flex-basis: 35%; */
                z-index: 3;
                display: inline-flex;
                flex-direction: column;
                align-items: flex-start;
                justify-content: flex-start;
                @media (max-width: 1440px) {
                }
                @media (max-width: 1024px) {
                    padding-bottom: 7rem;
                    text-align: center;
                    align-items: center;
                }
                @media (max-width: 769px) {
                    background: #021F56;
                    margin-bottom: 0;
                    padding-bottom:112px;
                    width:100%;
                }
                @media (max-width: 481px) {
                }
                @media (max-width: 376px) {
                    padding-bottom:90px;

                }
                .news-card-group {
                   
                    box-shadow: 0px 8px 180px rgba(17, 113, 167, 0.66);
                        
                }
                .s05-nav-button {
                    margin-top: 50px;
                }
                h2 {
                    text-align: left !important;
                    @media (max-width: 1024px) {
                        text-align: center !important;
                    }
                    @media (max-width: 769px) {
                    }
                    @media (max-width: 481px) {
                    }
                    @media (max-width: 376px) {
                    }
                }
                p {
                    display: flex;
                    align-items: center;
                    justify-content: flex-start;
                    width: 400px;
                    margin-bottom: 50px;
                    @media (max-width: 1440px) {
                    }
                    @media (max-width: 1024px) {
                        flex-basis: 100%;
                        text-align: center;
                        align-items: center;
                        justify-content: center;
                    }
                    @media (max-width: 769px) {
                    }
                    @media (max-width: 481px) {
                        font-weight:500;
                        width: 100%;
                    }
                    @media (max-width: 376px) {
                        /* width: 200px; */
                    }
                    span {
                        padding: 0 16px;
                    }
                }
                .button{
                    max-width:230px;
                    width:100%;
                }
            }
        }
    }
`;

const FoxChartSection = styled.section`
    &#foxChart {
        position: absolute;
        display: flex;
        justify-content: center;
        width: 100%;
        bottom: -255px;
        left: 0;
        z-index: 1;
        .container {
            position: relative;
            display: flex;
            justify-content: center;
        }

        @media (max-width: 1440px) {
        }
        @media (max-width: 1024px) {
        }
        @media (max-width: 769px) {
            bottom: -3rem;
            padding: 30px 14px;
        }
        @media (max-width: 481px) {
            padding: 0;
        }
        @media (max-width: 376px) {
        }
    }
`;
const FoxChartSectionContainer = () => {
    return (
        <FoxChartSection id="foxChart">
            <div className="container">
                <FoxChartBannerCard data={foxChart} />
            </div>
        </FoxChartSection>
    );
};

const PageContent = styled.div`
    color: var(--text-link);
    /* overflow-x: hidden; */
    & > section {
        animation: section-fade 500ms ease-in-out;
    }
`;

const IndexPage = () => {
    return (
        <Layout>
            <ThemeProvider theme={theme}>
                <SEO
                    title="Get started with Spectral today | Spectral"
                    description={s01.description}
                />
                <PageContent>
                    <Section01 id={s01.name}>
                        <div className="container ">
                            <HeaderAnimationCard data={s01.headerCard} />
                        </div>

                        <div className="s01-divide-container container flex center">
                            <div>
                                <h2>{s01.h2}</h2>
                            </div>
                        </div>
                        <div className="s01-promotion-container container flex center">
                            <div className="s01-promotion flex center">
                                {Object.values(s01.promotion).map((value, key) => (
                                    <PromotionCard
                                        key={key}
                                        index={key}
                                        className="space"
                                        data={{ ...value, imageSrc: config.svgs[value.imageSrc] }}
                                    />
                                ))}
                            </div>
                        </div>
                        <FoxChartSectionContainer />
                    </Section01>
                    <Section02 id={s02.name}>
                        <div className="s02-bg">
                            {/* {config.svgs['S02BgImage']} */}
                            {/* <img src={config.svgs['S02BgImage']} alt="s02" /> */}
                        </div>
                        <div className="container">
                            <IntroductionAnimationCard
                                isSvg
                                className="developerFirst"
                                data={s02.introduction.developerFirst}
                            />
                        </div>
                        <div className="s02-divider-container container">
                            <CardContent data={s02.divider} />

                            <div className="s02-divider-bg">
                                {/* {config.lazyImages['S02DividerImage']} */}
                                <img src={config.svgs['S02DividerImage']} alt="s02-divider" />
                                {/* {config.svgs['S02DividerImage']} */}
                                {/* {config.renderLotties['S02DividerImage']} */}
                            </div>
                        </div>
                        <div className="container">
                            <IntroductionAnimationCard
                                isSvg
                                className="production"
                                data={s02.introduction.production}
                                reverse
                            />
                        </div>
                    </Section02>
                    <Section03 id={s03.name}>
                        <div className="s03-container flex center container">
                            <h2>{s03.title}</h2>
                            <div className="s03-divide">
                                <img src={config.svgs['S03DividerImage']} alt="s03-divide" />
                            </div>
                            <div className="s03-image">
                                <img
                                    src={config.images['HomeS03ForAnyProgrammingImage']}
                                    alt="for any programming"
                                />
                            </div>
                        </div>
                    </Section03>
                    <Section04 id={s04.name}>
                        <div className="s04-container flex center container">
                            <h2>{s04.title}</h2>
                            <p>{s04.description}</p>
                            <StackServiceCard data={s04.stack} />
                            <div className="flex center button-group">
                                <ChakraHubspotFormDialog
                                    trigger={dialogProp => (
                                        <RadianButton {...dialogProp}>
                                            {s04.button.getStarted.text}
                                        </RadianButton>
                                    )}
                                />
                            </div>
                        </div>
                    </Section04>
                    <Section05 id={s05.name}>
                        <div className="s05-container flex center container">
                            <div className="s05-content">
                                <h2>{s05.title}</h2>
                                <p>{s05.description} </p>
                                <ChakraHubspotFormDialog
                                    trigger={dialogProp => (
                                        <RadianButton
                                            className="hover-dark-blue"
                                            color="white"
                                            variant="outline"
                                            {...dialogProp}
                                        >
                                            {s05.button.getStarted.text}
                                        </RadianButton>
                                    )}
                                />
                            </div>

                            <NewsGroupCard data={s05} />
                        </div>
                        <div className="s05-bg">
                            {/* <img
                                className="s05-bg-img"
                                src={config.svgs['S05Image']}
                                alt="s05-bg"
                            /> */}
                        </div>
                    </Section05>
                </PageContent>
            </ThemeProvider>
        </Layout>
    );
};

export default IndexPage;
